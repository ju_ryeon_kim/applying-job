package core;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {

	public static List<Map<String, String>> translateParams(HttpServletRequest req, String reqKey) {
		String paramsStr = req.getParameter(reqKey);
		String[] paramsStrArr = paramsStr.split(";");
		List<Map<String, String>> maps = new ArrayList<>();
		for (int i = 0; i < paramsStrArr.length - 1; i++) {
			StringTokenizer st = new StringTokenizer(paramsStrArr[i], ",");
			Map<String, String> map = new HashMap<>();
			while (st.hasMoreTokens()) {
				String keyValue = st.nextToken();
				int delimIndex = keyValue.indexOf(':');
				if (delimIndex >= 0) {
					String key = keyValue.substring(0, delimIndex).trim();
					String value = keyValue.substring(delimIndex + 1).trim();
					map.put(key, value);
				}
			}
			maps.add(map);
		}
		return maps;
	}

	public static String parseLastPath(HttpServletRequest req) throws UnsupportedEncodingException {
		String url = req.getRequestURI();
		return URLDecoder.decode(url.substring(url.lastIndexOf("/") +1).trim(), "utf-8");
	}

	public static String parseModuleName(HttpServletRequest req) throws UnsupportedEncodingException {
		String url = req.getRequestURI();
		return URLDecoder.decode(url.substring(url.indexOf("/") +1).trim(), "utf-8");
	}
}
