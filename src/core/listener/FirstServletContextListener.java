package core.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import job.applying.dao.applicant.ApplicantDao;

@WebListener
public class FirstServletContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println(this.getClass());
		ApplicantDao applicantDao = new ApplicantDao();
		applicantDao.remove("hunt");
	}

}
