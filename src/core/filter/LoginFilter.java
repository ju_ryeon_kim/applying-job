package core.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.service.user.UserService;
import job.applying.web.user.LoginUserServlet;
@WebFilter(filterName = "login", urlPatterns = { "/applicant/*", "/jobObjective/*" })
public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
	//로그인 확인
		System.out.println(this.getClass());
			HttpSession session = ((HttpServletRequest)req).getSession();
			if (!UserService.isLogin(session)) {
				((HttpServletResponse)resp).sendRedirect("/");
				return;
			}
			chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
