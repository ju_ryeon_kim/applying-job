package core.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.careerStatement.CareerStatement;
import job.applying.service.ExistData;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.applyingRoute.ApplyingRouteService;
import job.applying.service.award.AwardService;
import job.applying.service.careerStatement.CareerStatementService;
import job.applying.service.certificate.CertificateService;
import job.applying.service.computerSkill.ComputerSkillService;
import job.applying.service.dependent.DependentService;
import job.applying.service.militaryService.MilitaryServiceService;
import job.applying.service.project.ProjectService;
import job.applying.service.selfIntroduction.SelfIntroductionService;
import job.applying.service.workExperience.WorkExperienceService;

@WebFilter(filterName = "isApplicant", urlPatterns = { "/applicant/delete", "/applicant/read", "/jobObjective/*",
		"/education/*", "/language/*", "/certificate/*", "/militaryService/*", "/workExperience/*", "/award/*",
		"/dependent/*", "/computerSkill/*", "/applyingRoute/*", "/project/*", "/careerStatement/*", "/selfIntroduction/*", "/applyingRoute/*" })
public class ExistApplicantFilter implements Filter {

	List<ExistData> existDataChecker = new ArrayList<>();

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		System.out.println(this.getClass());
		HttpSession session = ((HttpServletRequest) req).getSession();
		if (!ApplicantService.exist(session)) {
			((HttpServletResponse) resp).sendRedirect("/");
			return;
		}
		String applicantId = SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID);
		for (ExistData ed : existDataChecker) {
			if (ed.exist(applicantId)) {
				session.setAttribute(ed.getSessionId(), applicantId);
			} else {
				session.removeAttribute(ed.getSessionId());
			}
		}

		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		existDataChecker.add(new CertificateService());
		existDataChecker.add(new MilitaryServiceService());
		existDataChecker.add(new WorkExperienceService());
		existDataChecker.add(new AwardService());
		existDataChecker.add(new DependentService());
		existDataChecker.add(new ComputerSkillService());
		existDataChecker.add(new ProjectService());
		existDataChecker.add(new CareerStatementService());
		existDataChecker.add(new SelfIntroductionService());
		existDataChecker.add(new ApplyingRouteService());
	}

}
