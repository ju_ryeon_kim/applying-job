package job.applying.service.militaryService;

import job.applying.dao.militaryService.MilitaryServiceDao;
import job.applying.domain.militaryService.MilitaryService;
import job.applying.service.ExistData;

public class MilitaryServiceService implements ExistData {
	private static final String SESSION_MILITARY_SERVICE_ID = "militaryServiceId";
	private static final String REQUEST_MILITARY_SERVICE = "militaryService";
	
	public void save(MilitaryService militaryService, String applicantId) {
		MilitaryServiceDao militaryServiceDao = new MilitaryServiceDao();
		militaryServiceDao.addOrUpdate(militaryService, applicantId);
	}

	@Override
	public boolean exist(String applicantId) {
		MilitaryServiceDao militaryServiceDao = new MilitaryServiceDao();
		int count = militaryServiceDao.count(applicantId);
		return count > 0;
	}

	@Override
	public String getSessionId() {
		return SESSION_MILITARY_SERVICE_ID;
	}

	public String getRequestKey() {
		return REQUEST_MILITARY_SERVICE;
	}

	public MilitaryService get(String applicantId) {
		MilitaryServiceDao militaryServiceDao = new MilitaryServiceDao();
		return militaryServiceDao.findById(applicantId);
	}

	public void delete(String applicantId) {
		// TODO Auto-generated method stub
		MilitaryServiceDao militaryServiceDao = new MilitaryServiceDao();
		militaryServiceDao.remove(applicantId);
	}

}
