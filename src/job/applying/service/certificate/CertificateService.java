package job.applying.service.certificate;

import java.util.List;

import job.applying.dao.certificate.CertificateDao;
import job.applying.domain.certificate.Certificate;
import job.applying.service.ExistData;

public class CertificateService implements ExistData {
	public static final String SESSION_CERTIFICATE_ID = "certificateId";
	public static final String REQUEST_CERTIFICATES = "certificates";

	public static void save(List<Certificate> certificates, String applicantId) {
		CertificateDao certificateDao = new CertificateDao();
		certificateDao.remove(applicantId);
		certificateDao.addOrUpdates(certificates, applicantId);
	}

	public static List<Certificate> getList(String applicantId) {
		CertificateDao certificateDao = new CertificateDao();
		return certificateDao.findListById(applicantId);
	}

	public static void delete(String applicantId) {
		CertificateDao certificateDao = new CertificateDao();
		certificateDao.remove(applicantId);
	}

	public static void delete(String applicantId, String certificateName) {
		CertificateDao certificateDao = new CertificateDao();
		certificateDao.remove(applicantId, certificateName);

	}

	@Override
	public boolean exist(String applicantId) {
		CertificateDao certificateDao = new CertificateDao();
		int count = certificateDao.count(applicantId);
		return count > 0;
	}

	@Override
	public String getSessionId() {
		return SESSION_CERTIFICATE_ID;
	}

	@Override
	public String getRequestKey() {
		return REQUEST_CERTIFICATES;
	}

}
