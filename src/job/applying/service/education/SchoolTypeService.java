package job.applying.service.education;

import java.util.List;

import job.applying.dao.education.SchoolTypeDao;
import job.applying.domain.education.SchoolType;

public class SchoolTypeService {
	
	public static final String REQUEST_SCHOOL_TYPES = "schoolTypes";
	
	public static List<SchoolType> getSchoolTypes() {
		SchoolTypeDao schoolTypeDao = new SchoolTypeDao();
		return schoolTypeDao.findAll();
	}
}
