package job.applying.service.education;

import java.util.List;

import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.education.EducationDao;
import job.applying.domain.education.Education;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.service.applicant.ApplicantService;

public class EducationService {
	public static final String SESSION_EDUCATION_ID = "educationId";
	public static final String REQUEST_EDUCATIONS = "educations";

	public static boolean exist(HttpSession session) {
		if (!SessionUtils.isEmpty(session, SESSION_EDUCATION_ID)) {
			return true;
		} else if (ApplicantService.exist(session)) {
			List<Education> educations = get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
			if (educations != null && educations.size() > 0) {
				session.setAttribute(SESSION_EDUCATION_ID, ApplicantService.SESSION_APPLICANT_ID);
				return true;
			}
		}
		return false;
	}

	public static List<Education> get(String applicantId) {
		EducationDao educationDao = new EducationDao();
		return educationDao.findById(applicantId);
	}

	public static void save(List<Education> educations, String applicantId) {
		EducationDao educationDao = new EducationDao();
		educationDao.remove(applicantId);
		educationDao.addOrUpdates(educations, applicantId);
	}
	public static void delete(String applicantId) {
		EducationDao educationDao = new EducationDao();
		educationDao.remove(applicantId);
	}

	public static void delete(String applicantId, int schoolId) {
		EducationDao educationDao = new EducationDao();
		educationDao.remove(applicantId, schoolId);
	}
}
