package job.applying.service.applyingRoute;

import java.util.List;

import job.applying.dao.applyingRoute.ApplyingRouteTypeDao;
import job.applying.dao.dependent.DependentDao;
import job.applying.domain.applyingRoute.ApplyingRouteType;

public class ApplyingRouteTypeService {
	private static final String REQUEST_KEY = "applyingRouteTypes";

	public List<ApplyingRouteType> get() {
		ApplyingRouteTypeDao dao = new ApplyingRouteTypeDao();
		return dao.findAll();
	}

	public String getRequestKey() {
		return REQUEST_KEY;
	}

}
