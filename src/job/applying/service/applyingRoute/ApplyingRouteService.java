package job.applying.service.applyingRoute;

import java.util.List;

import job.applying.dao.applyingRoute.ApplyingRouteDao;
import job.applying.dao.computerSkill.ComputerSkillDao;
import job.applying.domain.applyingRoute.ApplyingRoute;
import job.applying.service.ExistData;

public class ApplyingRouteService implements ExistData{
	private static final String SESSION_ID = "applyingRouteId";
	private static final String REQUEST_KEY = "applyingRoutes";

	@Override
	public boolean exist(String applicantId) {
		ApplyingRouteDao dao = new ApplyingRouteDao();
		return dao.count(applicantId) > 0;
	}

	@Override
	public String getSessionId() {
		return SESSION_ID;
	}

	@Override
	public String getRequestKey() {
		return REQUEST_KEY;
	}

	public List<ApplyingRoute> get(String applicantId) {
		ApplyingRouteDao dao = new ApplyingRouteDao();
		return dao.findListById(applicantId);
	}

	public void save(List<ApplyingRoute> list, String applicantId) {
		ApplyingRouteDao dao = new ApplyingRouteDao();
		dao.remove(applicantId);
		dao.addOrUpdates(list, applicantId);
	}

	public void delete(String applicantId, String key) {
		ApplyingRouteDao dao = new ApplyingRouteDao();
		dao.remove(applicantId, key);
	}

	public void delete(String applicantId) {
		ApplyingRouteDao dao = new ApplyingRouteDao();
		dao.remove(applicantId);
	}

}
