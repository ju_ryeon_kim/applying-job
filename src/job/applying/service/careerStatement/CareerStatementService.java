package job.applying.service.careerStatement;

import java.util.List;

import job.applying.dao.careerStatement.CareerStatementDao;
import job.applying.dao.project.ProjectDao;
import job.applying.domain.careerStatement.CareerStatement;
import job.applying.service.ExistData;

public class CareerStatementService implements ExistData {
	private static final String SESSION_ID = "careerStatementId";
	private static final String REQUEST_KEY = "careerStatements";

	@Override
	public boolean exist(String applicantId) {
		CareerStatementDao dao = new CareerStatementDao();
		return dao.count(applicantId) > 0;
	}

	@Override
	public String getSessionId() {
		return SESSION_ID;
	}

	@Override
	public String getRequestKey() {
		return REQUEST_KEY;
	}

	public List<CareerStatement> get(String applicantId) {
		CareerStatementDao dao = new CareerStatementDao();
		return dao.findListById(applicantId);
	}

	public void save(List<CareerStatement> list, String applicantId) {
		CareerStatementDao dao = new CareerStatementDao();
		dao.remove(applicantId);
		dao.addOrUpdates(list, applicantId);
	}

	public void delete(String applicantId, String key) {
		CareerStatementDao dao = new CareerStatementDao();
		dao.remove(applicantId, key);
	}

	public void delete(String applicantId) {
		CareerStatementDao dao = new CareerStatementDao();
		dao.remove(applicantId);
	}

}
