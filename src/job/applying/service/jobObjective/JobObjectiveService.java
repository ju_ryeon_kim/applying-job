package job.applying.service.jobObjective;

import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.jobObjective.JobObjectiveDao;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.user.UserService;

public class JobObjectiveService {

	public static final String SESSION_JOB_OBJECTIVE_ID = "jobObjectiveId";
	public static final String REQUEST_JOB_OBJECTIVE = "jobObjective";

	public static void save(JobObjective jobObjective, String applicantId) {
		JobObjectiveDao jobObjectiveDao = new JobObjectiveDao();
		jobObjectiveDao.addOrUpdate(jobObjective, applicantId);
	}

	public static JobObjective get(String applicantId) {
		JobObjectiveDao jobObjectiveDao = new JobObjectiveDao();
		return jobObjectiveDao.findById(applicantId);
	}

	public static void delete(String applicantId) {
		JobObjectiveDao jobObjectiveDao = new JobObjectiveDao();
		jobObjectiveDao.remove(applicantId);
	}

	public static boolean exist(HttpSession session) {
		if (!SessionUtils.isEmpty(session, SESSION_JOB_OBJECTIVE_ID)) {
			return true;
		} else if (ApplicantService.exist(session)) {
			JobObjective jobObjective = get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
			if (jobObjective != null) {
				session.setAttribute(SESSION_JOB_OBJECTIVE_ID, ApplicantService.SESSION_APPLICANT_ID);
				return true;
			}
		}
		return false;
	}

}
