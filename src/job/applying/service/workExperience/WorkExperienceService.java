package job.applying.service.workExperience;

import java.util.List;

import job.applying.dao.certificate.CertificateDao;
import job.applying.dao.workExperience.WorkExperienceDao;
import job.applying.domain.workExperience.WorkExperience;
import job.applying.service.ExistData;

public class WorkExperienceService implements ExistData {
	private static final String SESSION_WORK_EXPERIENCE_ID = "workExperienceId";
	private static final String REQUEST_WORK_EXPERIENCE_SERVICES = "workExperiences";

	public void save(List<WorkExperience> workExps, String applicantId) {
		WorkExperienceDao wed = new WorkExperienceDao();
		wed.remove(applicantId);
		wed.addOrUpdates(workExps, applicantId);
	}

	@Override
	public boolean exist(String applicantId) {
		WorkExperienceDao wed = new WorkExperienceDao();
		int count = wed.count(applicantId);
		return count > 0;

	}

	@Override
	public String getSessionId() {
		return SESSION_WORK_EXPERIENCE_ID;
	}

	@Override
	public String getRequestKey() {
		return REQUEST_WORK_EXPERIENCE_SERVICES;
	}

	public List<WorkExperience> get(String applicantId) {
		WorkExperienceDao wed = new WorkExperienceDao();
		return wed.findListById(applicantId);
	}

	public void delete(String applicantId) {
		WorkExperienceDao wed = new WorkExperienceDao();
		wed.remove(applicantId);
	}

	public void delete(String applicantId, String companyName) {
		WorkExperienceDao wed = new WorkExperienceDao();
		wed.remove(applicantId, companyName);
	}

}
