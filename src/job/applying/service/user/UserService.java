package job.applying.service.user;

import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.user.UserDao;
import job.applying.domain.user.PasswordMismatchException;
import job.applying.domain.user.User;
import job.applying.domain.user.UserNotFoundException;

public class UserService {
	public static boolean login(String userId, String password) throws UserNotFoundException, PasswordMismatchException {
		UserDao userDao = new UserDao();
		User user = userDao.findById(userId);
		if (user == null) {
			throw new UserNotFoundException();
		}
		if (!user.matchPassword(password)) {
			throw new PasswordMismatchException();
		}
		return true;
	}
	
	public static boolean login(User user) throws UserNotFoundException, PasswordMismatchException {
		return login(user.getUserId(), user.getPassword());
	}
	
	public static boolean isLogin(HttpSession session) {
		return !SessionUtils.isEmpty(session, UserService.SESSION_USER_ID);
	}

	public static void join(User user) throws UserIdAlreadyInUseException {
		UserDao userDao = new UserDao();
		User joinedUser = userDao.findById(user.getUserId());
		if (joinedUser != null) {
			throw new UserIdAlreadyInUseException();
		}
		userDao.addOrUpdate(user);
	}

	public static final String SESSION_USER_ID = "userId";

//	public static User getUser(String userId) {
//		UserDao userDao = new UserDao();
//		return userDao.findById(userId);
//	}
}
