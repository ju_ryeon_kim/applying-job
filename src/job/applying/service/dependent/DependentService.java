package job.applying.service.dependent;

import java.util.List;

import job.applying.dao.dependent.DependentDao;
import job.applying.domain.award.Award;
import job.applying.domain.dependent.Dependent;
import job.applying.service.ExistData;

public class DependentService implements ExistData {

	private static final String SESSION_ID = "dependentId";
	private static final String REQUEST_KEY = "dependents";

	public void save(List<Dependent> list, String applicantId) {
		DependentDao dependentDao = new DependentDao();
		dependentDao.remove(applicantId);
		dependentDao.addOrUpdates(list, applicantId);
	}

	@Override
	public boolean exist(String applicantId) {
		DependentDao dependentDao = new DependentDao();
		int count = dependentDao.count(applicantId);
		return count > 0;
	}

	@Override
	public String getSessionId() {
		// TODO Auto-generated method stub
		return SESSION_ID;
	}

	@Override
	public String getRequestKey() {
		// TODO Auto-generated method stub
		return REQUEST_KEY;
	}

	public List<Dependent> get(String applicantId) {
		DependentDao dependentDao = new DependentDao();
		return dependentDao.findListById(applicantId);
	}

	public void delete(String applicantId, String dependentName) {
		DependentDao dependentDao = new DependentDao();
		dependentDao.remove(applicantId, dependentName);
	}

	public void delete(String applicantId) {
		DependentDao dependentDao = new DependentDao();
		dependentDao.remove(applicantId);
	}
}
