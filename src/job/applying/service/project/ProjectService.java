package job.applying.service.project;

import java.util.List;

import job.applying.dao.computerSkill.ComputerSkillDao;
import job.applying.dao.project.ProjectDao;
import job.applying.domain.project.Project;
import job.applying.service.ExistData;

public class ProjectService implements ExistData{
	private static final String SESSION_ID = "projectId";
	private static final String REQUEST_KEY = "projects";

	@Override
	public boolean exist(String applicantId) {
		ProjectDao dao = new ProjectDao();
		return dao.count(applicantId) > 0;
	}

	@Override
	public String getSessionId() {
		return SESSION_ID;
	}

	@Override
	public String getRequestKey() {
		return REQUEST_KEY;
	}

	public List<Project> get(String applicantId) {
		ProjectDao dao = new ProjectDao();
		return dao.findListById(applicantId);
	}

	public void save(List<Project> list, String applicantId) {
		ProjectDao dao = new ProjectDao();
		dao.remove(applicantId);
		dao.addOrUpdates(list, applicantId);
	}

	public void delete(String applicantId, String key) {
		ProjectDao dao = new ProjectDao();
		dao.remove(applicantId, key);
	}

	public void delete(String applicantId) {
		ProjectDao dao = new ProjectDao();
		dao.remove(applicantId);
	}

}
