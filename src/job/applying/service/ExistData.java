package job.applying.service;

public interface ExistData {
	public boolean exist(String applicantId);
	public String getSessionId();
	public String getRequestKey();
}
