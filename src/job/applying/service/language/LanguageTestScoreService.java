package job.applying.service.language;

import java.util.List;

import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.language.LanguageTestScoreDao;
import job.applying.domain.education.Education;
import job.applying.domain.language.LanguageTestScore;
import job.applying.service.applicant.ApplicantService;

public class LanguageTestScoreService {

	public static final String SESSION_LANGUAGE_TEST_SCORE_ID = "languageTestScoreId";
	public static final String REQUEST_LANGUAGE_TEST_SCORES = "languageTestScores";

	public static void save(List<LanguageTestScore> languagetestScores, String applicantId) {
		LanguageTestScoreDao languageTestScoreDao = new LanguageTestScoreDao();
		languageTestScoreDao.remove(applicantId);
		languageTestScoreDao.addOrUpdates(languagetestScores, applicantId);
		
	}

	public static List<LanguageTestScore> get(String applicantId) {
		LanguageTestScoreDao languageTestScoreDao = new LanguageTestScoreDao();
		return languageTestScoreDao.findById(applicantId);
	}

	public static void delete(String applicantId, int languageTestId) {
		LanguageTestScoreDao languageTestScoreDao = new LanguageTestScoreDao();
		languageTestScoreDao.remove(applicantId, languageTestId);
	}

	public static void delete(String applicantId) {
		LanguageTestScoreDao languageTestScoreDao = new LanguageTestScoreDao();
		languageTestScoreDao.remove(applicantId);
	}

	public static boolean exist(HttpSession session) {
		if (!SessionUtils.isEmpty(session, SESSION_LANGUAGE_TEST_SCORE_ID)) {
			return true;
		} else if (ApplicantService.exist(session)) {
			List<LanguageTestScore> languageTestScores = get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
			if (languageTestScores != null && languageTestScores.size() > 0) {
				session.setAttribute(SESSION_LANGUAGE_TEST_SCORE_ID, ApplicantService.SESSION_APPLICANT_ID);
				return true;
			}
		}
		return false;
	}

}
