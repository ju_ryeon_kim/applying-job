package job.applying.service.language;

import java.util.List;

import job.applying.dao.language.LanguageTestDao;
import job.applying.domain.language.LanguageTest;
import job.applying.domain.language.LanguageTestScore;

public class LanguageTestService {

	public static final String REQUEST_LANGUAGE_TESTS = "languageTests";

	public static List<LanguageTest> getAll() {
		LanguageTestDao languageTestDao = new LanguageTestDao();
		return languageTestDao.findAll();
	}

	public static void save(LanguageTest languageTest) {
		LanguageTestDao languageTestDao = new LanguageTestDao();
		languageTestDao.addOrUpdate(languageTest);
	}
	
	public static LanguageTest get(String languageTestName) {
		LanguageTestDao languageTestDao = new LanguageTestDao();
		return languageTestDao.findByName(languageTestName);
	}

}
