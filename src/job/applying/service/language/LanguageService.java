package job.applying.service.language;

import java.util.List;

import job.applying.dao.language.LanguageDao;
import job.applying.domain.language.Language;

public class LanguageService {
//	public static final String SESSION_LANGUAGE_ID = "languageId";
	public static final String REQUEST_LANGUAGES = "languages";
	public static final String ETC = "기타";
	public static final int ETC_ID = 4;

	public static List<Language> get() {
		LanguageDao languageDao = new LanguageDao();
		return languageDao.selectAll();
	}
	
	public static boolean isEtc(String languageName) {
		return ETC.equals(languageName);
	}
	
	
}
