package job.applying.service.award;

import java.util.List;

import job.applying.dao.award.AwardServiceDao;
import job.applying.dao.militaryService.MilitaryServiceDao;
import job.applying.domain.award.Award;
import job.applying.service.ExistData;

public class AwardService implements ExistData {
	private static final String SESSION_AWARD_SERVICE_ID = "awardId";
	private static final String REQUEST_AWARD_SERVICES = "awards";

	public void save(List<Award> awards, String applicantId) {
		 AwardServiceDao awardServiceDao = new AwardServiceDao();
			AwardServiceDao.remove(applicantId);
		 AwardServiceDao.addOrUpdates(awards, applicantId);
	}

	@Override
	public boolean exist(String applicantId) {
		 AwardServiceDao awardServiceDao = new AwardServiceDao();
		 int count = AwardServiceDao.count(applicantId);
		return count > 0;
	}

	@Override
	public String getSessionId() {
		// TODO Auto-generated method stub
		return SESSION_AWARD_SERVICE_ID;
	}

	@Override
	public String getRequestKey() {
		// TODO Auto-generated method stub
		return REQUEST_AWARD_SERVICES;
	}

	public List<Award> get(String applicantId) {
		AwardServiceDao awardServiceDao = new AwardServiceDao();
		return AwardServiceDao.findListById(applicantId);
	}

	public void delete(String applicantId, String awardName) {
		AwardServiceDao awardServiceDao = new AwardServiceDao();
		AwardServiceDao.remove(applicantId, awardName);
	}

	public void delete(String applicantId) {
		AwardServiceDao awardServiceDao = new AwardServiceDao();
		AwardServiceDao.remove(applicantId);
	}

}
