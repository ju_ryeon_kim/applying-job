package job.applying.service.applicant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.applicant.ApplicantDao;
import job.applying.dao.applicant.ApplicantNotFoundException;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.user.User;
import job.applying.service.user.UserService;
import job.applying.web.user.LoginUserServlet;

public class ApplicantService {

	public static final String SESSION_APPLICANT_ID = "applicantId";
	public static final String REQUEST_APPLICANT = "applicant";

	public static Applicant get(String userId) {
		ApplicantDao applicantDao = new ApplicantDao();
		Applicant applicant = applicantDao.findById(userId);
		return applicant;
	}

	public static Applicant get(User user) {
		ApplicantDao applicantDao = new ApplicantDao();
		Applicant applicant = applicantDao.findById(user.getUserId());
		return applicant;
	}

	public static void save(Applicant applicant) {
		ApplicantDao applicantDao = new ApplicantDao();
		applicantDao.addOrUpdate(applicant);
	}

	public static void delete(String userId) {
		ApplicantDao applicantDao = new ApplicantDao();
		applicantDao.remove(userId);
	}

	public static boolean exist(HttpSession session) {
		if (!SessionUtils.isEmpty(session, SESSION_APPLICANT_ID)) {
			return true;
		} else if (UserService.isLogin(session)) {
			Applicant applicant = get(SessionUtils.getStringValue(session, UserService.SESSION_USER_ID));
			if (applicant != null) {
				session.setAttribute(SESSION_APPLICANT_ID, applicant.getUserId());
				return true;
			}
		}
		return false;
	}

}
