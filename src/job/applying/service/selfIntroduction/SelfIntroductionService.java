package job.applying.service.selfIntroduction;

import job.applying.dao.careerStatement.CareerStatementDao;
import job.applying.dao.selfIntroduction.SelfIntroductionDao;
import job.applying.domain.selfIntroduction.SelfIntroduction;
import job.applying.service.ExistData;

public class SelfIntroductionService  implements ExistData{
	private static final String SESSION_ID = "selfIntroductionId";
	private static final String REQUEST_KEY = "selfIntroduction";
	@Override
	public boolean exist(String applicantId) {
		SelfIntroductionDao dao = new SelfIntroductionDao();
		return dao.count(applicantId) > 0;
	}

	@Override
	public String getSessionId() {
		return SESSION_ID;
	}

	@Override
	public String getRequestKey() {
		return REQUEST_KEY;
	}

	public void save(SelfIntroduction item, String applicantId) {
		SelfIntroductionDao dao = new SelfIntroductionDao();
		dao.remove(applicantId);
		dao.addOrUpdate(item, applicantId);
	}

	public SelfIntroduction get(String applicantId) {
		SelfIntroductionDao dao = new SelfIntroductionDao();
		return dao.findById(applicantId);
	}

	public void delete(String applicantId) {
		SelfIntroductionDao dao = new SelfIntroductionDao();
		dao.remove(applicantId);
	}

}
