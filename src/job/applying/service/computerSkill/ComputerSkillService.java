package job.applying.service.computerSkill;

import java.util.List;

import job.applying.dao.computerSkill.ComputerSkillDao;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.dependent.Dependent;
import job.applying.service.ExistData;

public class ComputerSkillService implements ExistData {
	private static final String SESSION_ID = "computerSkillId";
	private static final String REQUEST_KEY = "computerSkills";
	@Override
	public boolean exist(String applicantId) {
		ComputerSkillDao dao = new ComputerSkillDao();
		return dao.count(applicantId) > 0;
	}

	public List<ComputerSkill> get(String applicantId) {
		ComputerSkillDao dao = new ComputerSkillDao();
		return dao.findListById(applicantId);
	}

	@Override
	public String getRequestKey() {
		return REQUEST_KEY;
	}

	@Override
	public String getSessionId() {
		return SESSION_ID;
	}

	public void save(List<ComputerSkill> list, String applicantId) {
		ComputerSkillDao dao = new ComputerSkillDao();
		dao.remove(applicantId);
		dao.addOrUpdates(list, applicantId);
	}

	public void delete(String applicantId) {
		ComputerSkillDao dao = new ComputerSkillDao();
		dao.remove(applicantId);
	}

	public void delete(String applicantId, String key) {
		ComputerSkillDao dao = new ComputerSkillDao();
		dao.remove(applicantId, key);
	}

}
