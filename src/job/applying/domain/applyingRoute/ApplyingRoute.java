package job.applying.domain.applyingRoute;

public class ApplyingRoute extends ApplyingRouteType {
	private String applyingRoute;

	public String getApplyingRoute() {
		return applyingRoute;
	}

	public void setApplyingRoute(String applyingRoute) {
		this.applyingRoute = applyingRoute;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applyingRoute == null) ? 0 : applyingRoute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplyingRoute other = (ApplyingRoute) obj;
		if (applyingRoute == null) {
			if (other.applyingRoute != null)
				return false;
		} else if (!applyingRoute.equals(other.applyingRoute))
			return false;
		return true;
	}

}
