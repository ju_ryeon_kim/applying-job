package job.applying.domain.careerStatement;

import java.time.LocalDate;

public class CareerStatement {
	private String company;
	private String department;
	private String position;
	private String companyStatus;
	private LocalDate projectStartDate;
	private LocalDate projectEndDate;
	private String mainBusiness;
	private LocalDate workStartDate;
	private LocalDate workEndDate;
	private String mainRole;
	private String businessPerformance;

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCompanyStatus() {
		return companyStatus;
	}

	public void setCompanyStatus(String companyStatus) {
		this.companyStatus = companyStatus;
	}

	public LocalDate getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(LocalDate projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public LocalDate getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(LocalDate projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public String getMainBusiness() {
		return mainBusiness;
	}

	public void setMainBusiness(String mainBusiness) {
		this.mainBusiness = mainBusiness;
	}

	public LocalDate getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(LocalDate workStartDate) {
		this.workStartDate = workStartDate;
	}

	public LocalDate getWorkEndDate() {
		return workEndDate;
	}

	public void setWorkEndDate(LocalDate workEndDate) {
		this.workEndDate = workEndDate;
	}

	public String getMainRole() {
		return mainRole;
	}

	public void setMainRole(String mainRole) {
		this.mainRole = mainRole;
	}

	public String getBusinessPerformance() {
		return businessPerformance;
	}

	public void setBusinessPerformance(String businessPerformance) {
		this.businessPerformance = businessPerformance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessPerformance == null) ? 0 : businessPerformance.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((companyStatus == null) ? 0 : companyStatus.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((mainBusiness == null) ? 0 : mainBusiness.hashCode());
		result = prime * result + ((mainRole == null) ? 0 : mainRole.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((projectEndDate == null) ? 0 : projectEndDate.hashCode());
		result = prime * result + ((projectStartDate == null) ? 0 : projectStartDate.hashCode());
		result = prime * result + ((workEndDate == null) ? 0 : workEndDate.hashCode());
		result = prime * result + ((workStartDate == null) ? 0 : workStartDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CareerStatement other = (CareerStatement) obj;
		if (businessPerformance == null) {
			if (other.businessPerformance != null)
				return false;
		} else if (!businessPerformance.equals(other.businessPerformance))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (companyStatus == null) {
			if (other.companyStatus != null)
				return false;
		} else if (!companyStatus.equals(other.companyStatus))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (mainBusiness == null) {
			if (other.mainBusiness != null)
				return false;
		} else if (!mainBusiness.equals(other.mainBusiness))
			return false;
		if (mainRole == null) {
			if (other.mainRole != null)
				return false;
		} else if (!mainRole.equals(other.mainRole))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (projectEndDate == null) {
			if (other.projectEndDate != null)
				return false;
		} else if (!projectEndDate.equals(other.projectEndDate))
			return false;
		if (projectStartDate == null) {
			if (other.projectStartDate != null)
				return false;
		} else if (!projectStartDate.equals(other.projectStartDate))
			return false;
		if (workEndDate == null) {
			if (other.workEndDate != null)
				return false;
		} else if (!workEndDate.equals(other.workEndDate))
			return false;
		if (workStartDate == null) {
			if (other.workStartDate != null)
				return false;
		} else if (!workStartDate.equals(other.workStartDate))
			return false;
		return true;
	}

}
