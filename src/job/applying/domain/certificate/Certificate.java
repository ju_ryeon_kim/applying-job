package job.applying.domain.certificate;

import java.time.LocalDate;

public class Certificate {
	private String certificateName;
	private String certificateAuthority;
	private LocalDate certificateAcquisitionDate;

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public String getCertificateAuthority() {
		return certificateAuthority;
	}

	public void setCertificateAuthority(String certificateAuthority) {
		this.certificateAuthority = certificateAuthority;
	}

	public LocalDate getCertificateAcquisitionDate() {
		return certificateAcquisitionDate;
	}

	public void setCertificateAcquisitionDate(LocalDate certificateAcquisitionDate) {
		this.certificateAcquisitionDate = certificateAcquisitionDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((certificateAcquisitionDate == null) ? 0 : certificateAcquisitionDate.hashCode());
		result = prime * result + ((certificateAuthority == null) ? 0 : certificateAuthority.hashCode());
		result = prime * result + ((certificateName == null) ? 0 : certificateName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Certificate other = (Certificate) obj;
		if (certificateAcquisitionDate == null) {
			if (other.certificateAcquisitionDate != null)
				return false;
		} else if (!certificateAcquisitionDate.equals(other.certificateAcquisitionDate))
			return false;
		if (certificateAuthority == null) {
			if (other.certificateAuthority != null)
				return false;
		} else if (!certificateAuthority.equals(other.certificateAuthority))
			return false;
		if (certificateName == null) {
			if (other.certificateName != null)
				return false;
		} else if (!certificateName.equals(other.certificateName))
			return false;
		return true;
	}

}
