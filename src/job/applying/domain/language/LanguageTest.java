package job.applying.domain.language;

public class LanguageTest extends Language {
	private int languageTestId;
	private String languageTestName;

	public int getLanguageTestId() {
		return languageTestId;
	}

	public void setLanguageTestId(int languageTestId) {
		this.languageTestId = languageTestId;
	}

	public String getLanguageTestName() {
		return languageTestName;
	}

	public void setLanguageTestName(String languageTestName) {
		this.languageTestName = languageTestName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + languageTestId;
		result = prime * result + ((languageTestName == null) ? 0 : languageTestName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LanguageTest other = (LanguageTest) obj;
		if (languageTestId != other.languageTestId)
			return false;
		if (languageTestName == null) {
			if (other.languageTestName != null)
				return false;
		} else if (!languageTestName.equals(other.languageTestName))
			return false;
		return true;
	}

}
