package job.applying.domain.language;

import java.time.LocalDate;

public class LanguageTestScore extends LanguageTest {
	private float languageTestScore;
	private LocalDate languageTestScoreAcquisitionDate;
	public float getLanguageTestScore() {
		return languageTestScore;
	}

	public void setLanguageTestScore(float languageTestScore) {
		this.languageTestScore = languageTestScore;
	}

	public LocalDate getLanguageTestScoreAcquisitionDate() {
		return languageTestScoreAcquisitionDate;
	}

	public void setLanguageTestScoreAcquisitionDate(LocalDate languageTestScoreAcquisitionDate) {
		this.languageTestScoreAcquisitionDate = languageTestScoreAcquisitionDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((languageTestScoreAcquisitionDate == null) ? 0 : languageTestScoreAcquisitionDate.hashCode());
		result = prime * result + Float.floatToIntBits(languageTestScore);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LanguageTestScore other = (LanguageTestScore) obj;
		if (languageTestScoreAcquisitionDate == null) {
			if (other.languageTestScoreAcquisitionDate != null)
				return false;
		} else if (!languageTestScoreAcquisitionDate.equals(other.languageTestScoreAcquisitionDate))
			return false;
		if (Float.floatToIntBits(languageTestScore) != Float.floatToIntBits(other.languageTestScore))
			return false;
		return true;
	}

}
