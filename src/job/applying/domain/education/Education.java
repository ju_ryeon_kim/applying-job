package job.applying.domain.education;

import java.time.LocalDate;

public class Education extends School {
	public Education(School school) {
		super(school.getSchoolName(), school.getSchoolLocation(), new SchoolType(school.getSchoolType()));
		this.setSchoolId(school.getSchoolId());
	}

	private LocalDate educationStart;
	private LocalDate educationEnd;
	private EducationDetail educationDetail;

	public EducationDetail getEducationDetail() {
		return educationDetail;
	}

	public void setEducationDetail(EducationDetail educationDetail) {
		this.educationDetail = educationDetail;
	}

	public LocalDate getEducationStart() {
		return educationStart;
	}

	public void setEducationStart(LocalDate educationStart) {
		this.educationStart = educationStart;
	}

	public LocalDate getEducationEnd() {
		return educationEnd;
	}

	public void setEducationEnd(LocalDate educationEnd) {
		this.educationEnd = educationEnd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((educationDetail == null) ? 0 : educationDetail.hashCode());
		result = prime * result + ((educationEnd == null) ? 0 : educationEnd.hashCode());
		result = prime * result + ((educationStart == null) ? 0 : educationStart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Education other = (Education) obj;
		if (educationDetail == null) {
			if (other.educationDetail != null)
				return false;
		} else if (!educationDetail.equals(other.educationDetail))
			return false;
		if (educationEnd == null) {
			if (other.educationEnd != null)
				return false;
		} else if (!educationEnd.equals(other.educationEnd))
			return false;
		if (educationStart == null) {
			if (other.educationStart != null)
				return false;
		} else if (!educationStart.equals(other.educationStart))
			return false;
		return true;
	}


	

}
