package job.applying.domain.education;

public class SchoolType {
	private String schoolType;

	public SchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	public String getSchoolType() {
		return schoolType;
	}

	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((schoolType == null) ? 0 : schoolType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SchoolType other = (SchoolType) obj;
		if (schoolType == null) {
			if (other.schoolType != null)
				return false;
		} else if (!schoolType.equals(other.schoolType))
			return false;
		return true;
	}


}
