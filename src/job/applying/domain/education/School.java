package job.applying.domain.education;

public class School extends SchoolType {
	private int schoolId;
	private String schoolName;
	private String schoolLocation;

	public School(SchoolType schoolType) {
		super(schoolType.getSchoolType());
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public School(String schoolName, String schoolLocation, SchoolType schoolType) {
		super(schoolType.getSchoolType());
		this.schoolName = schoolName;
		this.schoolLocation = schoolLocation;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolLocation() {
		return schoolLocation;
	}

	public void setSchoolLocation(String schoolLocation) {
		this.schoolLocation = schoolLocation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + schoolId;
		result = prime * result + ((schoolLocation == null) ? 0 : schoolLocation.hashCode());
		result = prime * result + ((schoolName == null) ? 0 : schoolName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		School other = (School) obj;
		if (schoolId != other.schoolId)
			return false;
		if (schoolLocation == null) {
			if (other.schoolLocation != null)
				return false;
		} else if (!schoolLocation.equals(other.schoolLocation))
			return false;
		if (schoolName == null) {
			if (other.schoolName != null)
				return false;
		} else if (!schoolName.equals(other.schoolName))
			return false;
		return true;
	}

}
