package job.applying.domain.education;

public class EducationDetail {
	private String educationMajor;
	private float educationCredit;
	private float educationFullMarks;

	public String getEducationMajor() {
		return educationMajor;
	}

	public void setEducationMajor(String educationMajor) {
		this.educationMajor = educationMajor;
	}

	public float getEducationCredit() {
		return educationCredit;
	}

	public void setEducationCredit(float educationCredit) {
		this.educationCredit = educationCredit;
	}

	public float getEducationFullMarks() {
		return educationFullMarks;
	}

	public void setEducationFullMarks(float educationFullMarks) {
		this.educationFullMarks = educationFullMarks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(educationCredit);
		result = prime * result + Float.floatToIntBits(educationFullMarks);
		result = prime * result + ((educationMajor == null) ? 0 : educationMajor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EducationDetail other = (EducationDetail) obj;
		if (Float.floatToIntBits(educationCredit) != Float.floatToIntBits(other.educationCredit))
			return false;
		if (Float.floatToIntBits(educationFullMarks) != Float.floatToIntBits(other.educationFullMarks))
			return false;
		if (educationMajor == null) {
			if (other.educationMajor != null)
				return false;
		} else if (!educationMajor.equals(other.educationMajor))
			return false;
		return true;
	}

}
