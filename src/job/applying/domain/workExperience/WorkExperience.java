package job.applying.domain.workExperience;

import java.time.LocalDate;

public class WorkExperience {
	private String companyName;
	private String departmentName;
	private String workName;
	private String positionName;
	private LocalDate workStartDate;
	private LocalDate workEndDate;
	private String retirementReason;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getWorkName() {
		return workName;
	}

	public void setWorkName(String workName) {
		this.workName = workName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public LocalDate getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(LocalDate workStartDate) {
		this.workStartDate = workStartDate;
	}

	public LocalDate getWorkEndDate() {
		return workEndDate;
	}

	public void setWorkEndDate(LocalDate workEndDate) {
		this.workEndDate = workEndDate;
	}

	public String getRetirementReason() {
		return retirementReason;
	}

	public void setRetirementReason(String retirementReason) {
		this.retirementReason = retirementReason;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((departmentName == null) ? 0 : departmentName.hashCode());
		result = prime * result + ((positionName == null) ? 0 : positionName.hashCode());
		result = prime * result + ((retirementReason == null) ? 0 : retirementReason.hashCode());
		result = prime * result + ((workEndDate == null) ? 0 : workEndDate.hashCode());
		result = prime * result + ((workName == null) ? 0 : workName.hashCode());
		result = prime * result + ((workStartDate == null) ? 0 : workStartDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkExperience other = (WorkExperience) obj;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (departmentName == null) {
			if (other.departmentName != null)
				return false;
		} else if (!departmentName.equals(other.departmentName))
			return false;
		if (positionName == null) {
			if (other.positionName != null)
				return false;
		} else if (!positionName.equals(other.positionName))
			return false;
		if (retirementReason == null) {
			if (other.retirementReason != null)
				return false;
		} else if (!retirementReason.equals(other.retirementReason))
			return false;
		if (workEndDate == null) {
			if (other.workEndDate != null)
				return false;
		} else if (!workEndDate.equals(other.workEndDate))
			return false;
		if (workName == null) {
			if (other.workName != null)
				return false;
		} else if (!workName.equals(other.workName))
			return false;
		if (workStartDate == null) {
			if (other.workStartDate != null)
				return false;
		} else if (!workStartDate.equals(other.workStartDate))
			return false;
		return true;
	}

}
