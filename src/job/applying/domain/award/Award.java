package job.applying.domain.award;

import java.time.LocalDate;

public class Award {
	private String awardName;
	private String awardGrade;
	private String awardAuthority;
	private LocalDate awardAcquisitionDate;

	public String getAwardName() {
		return awardName;
	}

	public void setAwardName(String awardName) {
		this.awardName = awardName;
	}

	public String getAwardGrade() {
		return awardGrade;
	}

	public void setAwardGrade(String awardGrade) {
		this.awardGrade = awardGrade;
	}

	public String getAwardAuthority() {
		return awardAuthority;
	}

	public void setAwardAuthority(String awardAuthority) {
		this.awardAuthority = awardAuthority;
	}

	public LocalDate getAwardAcquisitionDate() {
		return awardAcquisitionDate;
	}

	public void setAwardAcquisitionDate(LocalDate awardAcquisitionDate) {
		this.awardAcquisitionDate = awardAcquisitionDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((awardAcquisitionDate == null) ? 0 : awardAcquisitionDate.hashCode());
		result = prime * result + ((awardAuthority == null) ? 0 : awardAuthority.hashCode());
		result = prime * result + ((awardGrade == null) ? 0 : awardGrade.hashCode());
		result = prime * result + ((awardName == null) ? 0 : awardName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Award other = (Award) obj;
		if (awardAcquisitionDate == null) {
			if (other.awardAcquisitionDate != null)
				return false;
		} else if (!awardAcquisitionDate.equals(other.awardAcquisitionDate))
			return false;
		if (awardAuthority == null) {
			if (other.awardAuthority != null)
				return false;
		} else if (!awardAuthority.equals(other.awardAuthority))
			return false;
		if (awardGrade == null) {
			if (other.awardGrade != null)
				return false;
		} else if (!awardGrade.equals(other.awardGrade))
			return false;
		if (awardName == null) {
			if (other.awardName != null)
				return false;
		} else if (!awardName.equals(other.awardName))
			return false;
		return true;
	}

}
