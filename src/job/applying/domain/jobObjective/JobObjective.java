package job.applying.domain.jobObjective;

public class JobObjective {
	private String applyingField;
	private int annualSalary;
	private int desiredAnnualSalary;

	public int getDesiredAnnualSalary() {
		return desiredAnnualSalary;
	}

	public void setDesiredAnnualSalary(int desiredAnnualSalary) {
		this.desiredAnnualSalary = desiredAnnualSalary;
	}

	public JobObjective() {
	}

	public String getApplyingField() {
		return applyingField;
	}

	public void setApplyingField(String applyingField) {
		this.applyingField = applyingField;
	}

	public int getAnnualSalary() {
		return annualSalary;
	}

	public void setAnnualSalary(int annualSalary) {
		this.annualSalary = annualSalary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + annualSalary;
		result = prime * result + ((applyingField == null) ? 0 : applyingField.hashCode());
		result = prime * result + desiredAnnualSalary;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobObjective other = (JobObjective) obj;
		if (annualSalary != other.annualSalary)
			return false;
		if (applyingField == null) {
			if (other.applyingField != null)
				return false;
		} else if (!applyingField.equals(other.applyingField))
			return false;
		if (desiredAnnualSalary != other.desiredAnnualSalary)
			return false;
		return true;
	}

}
