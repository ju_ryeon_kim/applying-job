package job.applying.domain.applicant;

import java.time.LocalDate;
import java.util.List;

import job.applying.dao.applicant.ApplicantDao;
import job.applying.domain.education.Education;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.domain.user.User;

public class Applicant extends User {
	// 직접필드
	private String koreanName;
	private String chineseName;
	private LocalDate birthDate;
	private String address;
	private String bonjuck;
	private String phoneNumber;
	private String cellPhoneNumber;
	private String email;
	private boolean whetherVeteran;
	private boolean whetherDisabled;
	private LocalDate canJoinDate;

//	// 간접필드
//	private JobObjective jobObjective;
//	private List<Education> educations;

//	public List<Education> getEducations() {
//		return educations;
//	}
//
//	public void setEducations(List<Education> educations) {
//		this.educations = educations;
//	}
//
//	public JobObjective getJobObjective() {
//		return jobObjective;
//	}
//
//	public void setJobObjective(JobObjective jobObjective) {
//		this.jobObjective = jobObjective;
//	}

	public Applicant(User user) {
		super(user.getUserId(), user.getPassword());
	}

	public String getKoreanName() {
		return koreanName;
	}

	public void setKoreanName(String koreanName) {
		this.koreanName = koreanName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBonjuck() {
		return bonjuck;
	}

	public void setBonjuck(String bonjuck) {
		this.bonjuck = bonjuck;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isWhetherVeteran() {
		return whetherVeteran;
	}

	public void setWhetherVeteran(boolean whetherVeteran) {
		this.whetherVeteran = whetherVeteran;
	}

	public boolean isWhetherDisabled() {
		return whetherDisabled;
	}

	public void setWhetherDisabled(boolean whetherDisabled) {
		this.whetherDisabled = whetherDisabled;
	}

	public LocalDate getCanJoinDate() {
		return canJoinDate;
	}

	public void setCanJoinDate(LocalDate canJoinDate) {
		this.canJoinDate = canJoinDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((bonjuck == null) ? 0 : bonjuck.hashCode());
		result = prime * result + ((canJoinDate == null) ? 0 : canJoinDate.hashCode());
		result = prime * result + ((cellPhoneNumber == null) ? 0 : cellPhoneNumber.hashCode());
		result = prime * result + ((chineseName == null) ? 0 : chineseName.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((koreanName == null) ? 0 : koreanName.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + (whetherDisabled ? 1231 : 1237);
		result = prime * result + (whetherVeteran ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Applicant other = (Applicant) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (bonjuck == null) {
			if (other.bonjuck != null)
				return false;
		} else if (!bonjuck.equals(other.bonjuck))
			return false;
		if (canJoinDate == null) {
			if (other.canJoinDate != null)
				return false;
		} else if (!canJoinDate.equals(other.canJoinDate))
			return false;
		if (cellPhoneNumber == null) {
			if (other.cellPhoneNumber != null)
				return false;
		} else if (!cellPhoneNumber.equals(other.cellPhoneNumber))
			return false;
		if (chineseName == null) {
			if (other.chineseName != null)
				return false;
		} else if (!chineseName.equals(other.chineseName))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (koreanName == null) {
			if (other.koreanName != null)
				return false;
		} else if (!koreanName.equals(other.koreanName))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (whetherDisabled != other.whetherDisabled)
			return false;
		if (whetherVeteran != other.whetherVeteran)
			return false;
		return true;
	}



}
