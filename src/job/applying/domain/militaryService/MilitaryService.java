package job.applying.domain.militaryService;

public class MilitaryService {
	private String militaryServiceStatus;
	private String militaryServiceBranchName;
	private String militaryDivisionName;
	private String rankingName;
	private String completionReason;
	private int servicePeriod;
	private String exemptionReason;

	public String getMilitaryServiceStatus() {
		return militaryServiceStatus;
	}

	public void setMilitaryServiceStatus(String militaryServiceStatus) {
		this.militaryServiceStatus = militaryServiceStatus;
	}

	public String getMilitaryServiceBranchName() {
		return militaryServiceBranchName;
	}

	public void setMilitaryServiceBranchName(String militaryServiceBranchName) {
		this.militaryServiceBranchName = militaryServiceBranchName;
	}

	public String getMilitaryDivisionName() {
		return militaryDivisionName;
	}

	public void setMilitaryDivisionName(String militaryDivisionName) {
		this.militaryDivisionName = militaryDivisionName;
	}

	public String getRankingName() {
		return rankingName;
	}

	public void setRankingName(String rankingName) {
		this.rankingName = rankingName;
	}

	public String getCompletionReason() {
		return completionReason;
	}

	public void setCompletionReason(String completionReason) {
		this.completionReason = completionReason;
	}

	public int getServicePeriod() {
		return servicePeriod;
	}

	public void setServicePeriod(int servicePeriod) {
		this.servicePeriod = servicePeriod;
	}

	public String getExemptionReason() {
		return exemptionReason;
	}

	public void setExemptionReason(String exemptionReason) {
		this.exemptionReason = exemptionReason;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((completionReason == null) ? 0 : completionReason.hashCode());
		result = prime * result + ((exemptionReason == null) ? 0 : exemptionReason.hashCode());
		result = prime * result + ((militaryDivisionName == null) ? 0 : militaryDivisionName.hashCode());
		result = prime * result + ((militaryServiceBranchName == null) ? 0 : militaryServiceBranchName.hashCode());
		result = prime * result + ((militaryServiceStatus == null) ? 0 : militaryServiceStatus.hashCode());
		result = prime * result + ((rankingName == null) ? 0 : rankingName.hashCode());
		result = prime * result + servicePeriod;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MilitaryService other = (MilitaryService) obj;
		if (completionReason == null) {
			if (other.completionReason != null)
				return false;
		} else if (!completionReason.equals(other.completionReason))
			return false;
		if (exemptionReason == null) {
			if (other.exemptionReason != null)
				return false;
		} else if (!exemptionReason.equals(other.exemptionReason))
			return false;
		if (militaryDivisionName == null) {
			if (other.militaryDivisionName != null)
				return false;
		} else if (!militaryDivisionName.equals(other.militaryDivisionName))
			return false;
		if (militaryServiceBranchName == null) {
			if (other.militaryServiceBranchName != null)
				return false;
		} else if (!militaryServiceBranchName.equals(other.militaryServiceBranchName))
			return false;
		if (militaryServiceStatus == null) {
			if (other.militaryServiceStatus != null)
				return false;
		} else if (!militaryServiceStatus.equals(other.militaryServiceStatus))
			return false;
		if (rankingName == null) {
			if (other.rankingName != null)
				return false;
		} else if (!rankingName.equals(other.rankingName))
			return false;
		if (servicePeriod != other.servicePeriod)
			return false;
		return true;
	}

}
