package job.applying.domain.selfIntroduction;

public class SelfIntroduction {
	private String background;
	private String schoolLife;
	private String careerDetail;
	private String strengthsWeaknesses;
	private String mottoAndAim;
	private String purposeAndDream;

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getSchoolLife() {
		return schoolLife;
	}

	public void setSchoolLife(String schoolLife) {
		this.schoolLife = schoolLife;
	}

	public String getCareerDetail() {
		return careerDetail;
	}

	public void setCareerDetail(String careerDetail) {
		this.careerDetail = careerDetail;
	}

	public String getStrengthsWeaknesses() {
		return strengthsWeaknesses;
	}

	public void setStrengthsWeaknesses(String strengthsWeaknesses) {
		this.strengthsWeaknesses = strengthsWeaknesses;
	}

	public String getMottoAndAim() {
		return mottoAndAim;
	}

	public void setMottoAndAim(String mottoAndAim) {
		this.mottoAndAim = mottoAndAim;
	}

	public String getPurposeAndDream() {
		return purposeAndDream;
	}

	public void setPurposeAndDream(String purposeAndDream) {
		this.purposeAndDream = purposeAndDream;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((background == null) ? 0 : background.hashCode());
		result = prime * result + ((careerDetail == null) ? 0 : careerDetail.hashCode());
		result = prime * result + ((mottoAndAim == null) ? 0 : mottoAndAim.hashCode());
		result = prime * result + ((purposeAndDream == null) ? 0 : purposeAndDream.hashCode());
		result = prime * result + ((schoolLife == null) ? 0 : schoolLife.hashCode());
		result = prime * result + ((strengthsWeaknesses == null) ? 0 : strengthsWeaknesses.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SelfIntroduction other = (SelfIntroduction) obj;
		if (background == null) {
			if (other.background != null)
				return false;
		} else if (!background.equals(other.background))
			return false;
		if (careerDetail == null) {
			if (other.careerDetail != null)
				return false;
		} else if (!careerDetail.equals(other.careerDetail))
			return false;
		if (mottoAndAim == null) {
			if (other.mottoAndAim != null)
				return false;
		} else if (!mottoAndAim.equals(other.mottoAndAim))
			return false;
		if (purposeAndDream == null) {
			if (other.purposeAndDream != null)
				return false;
		} else if (!purposeAndDream.equals(other.purposeAndDream))
			return false;
		if (schoolLife == null) {
			if (other.schoolLife != null)
				return false;
		} else if (!schoolLife.equals(other.schoolLife))
			return false;
		if (strengthsWeaknesses == null) {
			if (other.strengthsWeaknesses != null)
				return false;
		} else if (!strengthsWeaknesses.equals(other.strengthsWeaknesses))
			return false;
		return true;
	}

}
