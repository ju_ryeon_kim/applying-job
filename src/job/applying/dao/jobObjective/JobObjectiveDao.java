package job.applying.dao.jobObjective;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.jobObjective.JobObjective;

public class JobObjectiveDao {

	public void addOrUpdate(JobObjective jobObjective, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE INTO job_objectives " + "(applicant_id, " + "applying_field, " + "annual_salary, "
				+ "desired_annual_salary)" + "VALUES(?, ?, ? ,?)";
		template.executeUpdate(sql, applicantId, jobObjective.getApplyingField(),
				jobObjective.getAnnualSalary(), jobObjective.getDesiredAnnualSalary());
	}

	public JobObjective findById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<JobObjective> rm = new RowMapper<JobObjective>() {
			@Override
			public JobObjective mapRow(ResultSet rs) throws SQLException {
				JobObjective jobObjective = new JobObjective();
				jobObjective.setApplyingField(rs.getString("applying_field"));
				jobObjective.setAnnualSalary(rs.getInt("annual_salary"));
				jobObjective.setDesiredAnnualSalary(rs.getInt("desired_annual_salary"));
				return jobObjective;
			}
		};
		String sql = "SELECT * FROM job_objectives WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM job_objectives WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

//	@Override
//	public void addOrUpdate(JobObjective jobObjective) {
//		JdbcTemplate template = new JdbcTemplate();
//		String sql = "UPDATE job_objectives SET "
//				+ "applying_field = ?, annual_salary = ?, desired_annual_salary = ? WHERE applicant_id = ?";
//		template.executeUpdate(sql, jobObjective.getApplyingField(), jobObjective.getAnnualSalary(),
//				jobObjective.getDesiredannualSalary(), jobObjective.getApplicantId());
//	}

	public List<JobObjective> findAll() {
		return null;
	}

	public boolean hasRow(String applicantId) throws JobObjectiveNotFoundException {
		JobObjective jobObjective = findById(applicantId);
		if (jobObjective == null) {
			throw new JobObjectiveNotFoundException();
		}
		return true;
	}

}

