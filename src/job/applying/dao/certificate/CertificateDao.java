package job.applying.dao.certificate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.certificate.Certificate;

public class CertificateDao {

	public void addOrUpdates(List<Certificate> certificates, String applicantId) {
		for (Certificate certificate : certificates) {
			addOrUpdate(certificate, applicantId);
		}
	}

	private void addOrUpdate(Certificate certificate, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE certificates (applicant_id, certificate_name, certificate_authority, certificate_acquisition_date) VALUES (?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, certificate.getCertificateName(), certificate.getCertificateAuthority(),
				certificate.getCertificateAcquisitionDate());
	}

	public List<Certificate> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Certificate> rm = new RowMapper<Certificate>() {
			@Override
			public Certificate mapRow(ResultSet rs) throws SQLException {
				Certificate certificate = new Certificate();
				certificate.setCertificateName(rs.getString("certificate_name"));
				certificate.setCertificateAuthority(rs.getString("certificate_authority"));
				certificate.setCertificateAcquisitionDate(rs.getDate("certificate_acquisition_date").toLocalDate());
				return certificate;
			}
		};
		String sql = "SELECT * FROM certificates WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM certificates WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void remove(String applicantId, String certificateName) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM certificates WHERE applicant_id = ? AND certificate_name = ?";
		template.executeUpdate(sql, applicantId, certificateName);
	}

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT COUNT(*) FROM certificates WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

}
