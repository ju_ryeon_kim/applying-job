package job.applying.dao.workExperience;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.certificate.Certificate;
import job.applying.domain.workExperience.WorkExperience;

public class WorkExperienceDao {

	public void addOrUpdates(List<WorkExperience> workExps, String applicantId) {
		for (WorkExperience we : workExps) {
			addOrUpdate(we, applicantId);
		}
	}

	private void addOrUpdate(WorkExperience we, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE work_experiences (applicant_id, company_name, department_name, work_name, position_name, work_start_date, work_end_date, retirement_reason) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, we.getCompanyName(), we.getDepartmentName(), we.getWorkName(),
				we.getPositionName(), we.getWorkStartDate(), we.getWorkEndDate(), we.getRetirementReason());
	}

	public List<WorkExperience> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<WorkExperience> rm = new RowMapper<WorkExperience>() {
			@Override
			public WorkExperience mapRow(ResultSet rs) throws SQLException {
				WorkExperience workExp = new WorkExperience();
				workExp.setCompanyName(rs.getString("company_name"));
				workExp.setDepartmentName(rs.getString("department_name"));
				workExp.setWorkName(rs.getString("work_name"));
				workExp.setPositionName(rs.getString("position_name"));
				workExp.setWorkStartDate(rs.getDate("work_start_date").toLocalDate());
				workExp.setWorkEndDate(rs.getDate("work_end_date").toLocalDate());
				workExp.setRetirementReason(rs.getString("retirement_reason"));
				return workExp;
			}
		};
		String sql = "SELECT * FROM work_experiences WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM work_experiences WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void remove(String applicantId, String companyName) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM work_experiences WHERE applicant_id = ? AND company_name = ?";
		template.executeUpdate(sql, applicantId, companyName);
	}

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT COUNT(*) FROM work_experiences WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

}
