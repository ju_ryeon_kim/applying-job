package job.applying.dao.militaryService;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.militaryService.MilitaryService;

public class MilitaryServiceDao {

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT COUNT(*) FROM military_services WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void addOrUpdate(MilitaryService militaryService, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE military_services (applicant_id, military_service_status, military_service_branch_name, "
				+ "military_division_name, ranking_name, completion_reason, service_period, exemption_reason) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, militaryService.getMilitaryServiceStatus(), militaryService.getMilitaryServiceBranchName(), militaryService.getMilitaryDivisionName(), 
				militaryService.getRankingName(), militaryService.getCompletionReason(), militaryService.getServicePeriod(), militaryService.getExemptionReason());
	}

	public MilitaryService findById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<MilitaryService> rm = new RowMapper<MilitaryService>() {
			@Override
			public MilitaryService mapRow(ResultSet rs) throws SQLException {
				MilitaryService ms = new MilitaryService();
				ms.setMilitaryServiceStatus(rs.getString("military_service_status"));
				ms.setMilitaryServiceBranchName(rs.getString("military_service_branch_name"));
				ms.setMilitaryDivisionName(rs.getString("military_division_name"));
				ms.setRankingName(rs.getString("ranking_name"));
				ms.setCompletionReason(rs.getString("completion_reason"));
				ms.setServicePeriod(rs.getInt("service_period"));
				ms.setExemptionReason(rs.getString("exemption_reason"));
				return ms;
			}
		};
		String sql = "SELECT * FROM military_services WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM military_services WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

}
