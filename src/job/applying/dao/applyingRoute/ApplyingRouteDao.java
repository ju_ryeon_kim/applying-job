package job.applying.dao.applyingRoute;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.applyingRoute.ApplyingRoute;
import job.applying.domain.computerSkill.ComputerSkill;

public class ApplyingRouteDao {
	private String tableName = "applying_routes";

	public void addOrUpdates(List<ApplyingRoute> list, String applicantId) {
		for (ApplyingRoute item : list) {
			addOrUpdate(item, applicantId);
		}
	}

	private void addOrUpdate(ApplyingRoute item, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE " + tableName + " (applicant_id, applying_route_type_id, applying_route) VALUES (?, ?, ?)";
		template.executeUpdate(sql, applicantId, item.getId(), item.getApplyingRoute());
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void remove(String applicantId, String key) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ? AND applying_route = ?";
		template.executeUpdate(sql, applicantId, key);
	}

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public List<ApplyingRoute> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<ApplyingRoute> rm = new RowMapper<ApplyingRoute>() {
			@Override
			public ApplyingRoute mapRow(ResultSet rs) throws SQLException {
				ApplyingRoute item = new ApplyingRoute();
				item.setId(rs.getInt("applying_route_type_id"));
				item.setApplyingRoute(rs.getString("applying_route"));
				item.setType(rs.getString("applying_route_type"));
				return item;
			}
		};
		String sql = "SELECT A.applying_route_type_id, A.applying_route, B.applying_route_type FROM " + tableName
				+ " A JOIN "
				+ " applying_route_types B ON A.applying_route_type_id = B.applying_route_type_id WHERE  applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}
}
