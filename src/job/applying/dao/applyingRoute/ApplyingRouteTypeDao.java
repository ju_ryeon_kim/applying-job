package job.applying.dao.applyingRoute;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.applyingRoute.ApplyingRouteType;

public class ApplyingRouteTypeDao {
	private String tableName = "applying_route_types";

	public List<ApplyingRouteType> findAll() {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<ApplyingRouteType> rm = new RowMapper<ApplyingRouteType>() {
			@Override
			public ApplyingRouteType mapRow(ResultSet rs) throws SQLException {
				ApplyingRouteType item = new ApplyingRouteType();
				item.setId(rs.getInt("applying_route_type_id"));
				item.setType(rs.getString("applying_route_type"));
				return item;
			}
		};
		String sql = "SELECT * FROM " + tableName;
		return template.list(sql, rm);
	}

}
