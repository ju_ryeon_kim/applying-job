package job.applying.dao.careerStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.careerStatement.CareerStatement;
import job.applying.domain.project.Project;

public class CareerStatementDao {
	private String tableName = "career_statements";

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void addOrUpdates(List<CareerStatement> list, String applicantId) {
		for (CareerStatement item : list) {
			addOrUpdate(item, applicantId);
		}
	}

	private void addOrUpdate(CareerStatement item, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE " + tableName
				+ " (applicant_id, company, department, career_position, company_status, project_start_date, project_end_date, main_business, work_start_date, work_end_date, main_role, business_performance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, item.getCompany(), item.getDepartment(), item.getPosition(),
				item.getCompanyStatus(), item.getProjectStartDate(), item.getProjectEndDate(), item.getMainBusiness(),
				item.getWorkStartDate(), item.getWorkEndDate(), item.getMainRole(), item.getBusinessPerformance());
	}

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public List<CareerStatement> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<CareerStatement> rm = new RowMapper<CareerStatement>() {
			@Override
			public CareerStatement mapRow(ResultSet rs) throws SQLException {
				CareerStatement item = new CareerStatement();
				item.setCompany(rs.getString("company"));
				item.setDepartment(rs.getString("department"));
				item.setPosition(rs.getString("career_position"));
				item.setCompanyStatus(rs.getString("company_status"));
				item.setProjectStartDate(rs.getDate("project_start_date").toLocalDate());
				item.setProjectEndDate(rs.getDate("project_end_date").toLocalDate());
				item.setMainBusiness(rs.getString("main_business"));
				item.setWorkStartDate(rs.getDate("work_start_date").toLocalDate());
				item.setWorkEndDate(rs.getDate("work_end_date").toLocalDate());
				item.setMainRole(rs.getString("main_role"));
				item.setBusinessPerformance(rs.getString("business_performance"));
				return item;
			}
		};
		String sql = "SELECT * FROM " + tableName + " WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public void remove(String applicantId, String key) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ? AND main_business = ?";
		template.executeUpdate(sql, applicantId, key);
	}

}
