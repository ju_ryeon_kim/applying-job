package job.applying.dao.award;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.award.Award;

public class AwardServiceDao {

	public static void addOrUpdates(List<Award> awards, String applicantId) {
		for (Award award : awards) {
			addOrUpdate(award, applicantId);
		}
	}

	private static void addOrUpdate(Award award, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE awards (applicant_id, award_name, award_grade, award_authority, award_acquisition_date) VALUES (?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, award.getAwardName(), award.getAwardGrade(), award.getAwardAuthority(),
				award.getAwardAcquisitionDate());
	}

	public static List<Award> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Award> rm = new RowMapper<Award>() {
			@Override
			public Award mapRow(ResultSet rs) throws SQLException {
				Award award = new Award();
				award.setAwardName(rs.getString("award_name"));
				award.setAwardGrade(rs.getString("award_grade"));
				award.setAwardAuthority(rs.getString("award_authority"));
				award.setAwardAcquisitionDate(rs.getDate("award_acquisition_date").toLocalDate());
				return award;
			}
		};
		String sql = "SELECT * FROM awards WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public static int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM awards WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public static void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM awards WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public static void remove(String applicantId, String awardName) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM awards WHERE applicant_id = ? AND award_name = ?";
		template.executeUpdate(sql, applicantId, awardName);
	}

}
