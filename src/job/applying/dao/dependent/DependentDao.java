package job.applying.dao.dependent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.award.Award;
import job.applying.domain.dependent.Dependent;

public class DependentDao {
	private String tableName = "dependents";

	public void addOrUpdates(List<Dependent> list, String applicantId) {
		for (Dependent item : list) {
			addOrUpdate(item, applicantId);
		}
	}

	private void addOrUpdate(Dependent item, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE " + tableName
				+ " (applicant_id, dependent_name, relation_ship, birth_date, education, job, living_together, dependent, company ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, item.getDependentName(), item.getRelationShip(), item.getBirthDate(),
				item.getEducation(), item.getJob(), item.isLivingTogether(), item.isDependent(), item.getCompany());
	}

	public List<Dependent> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Dependent> rm = new RowMapper<Dependent>() {
			@Override
			public Dependent mapRow(ResultSet rs) throws SQLException {
				Dependent dependent = new Dependent();
				dependent.setDependentName(rs.getString("dependent_name"));
				dependent.setRelationShip(rs.getString("relation_ship"));
				dependent.setBirthDate(rs.getDate("birth_date").toLocalDate());
				dependent.setEducation(rs.getString("education"));
				dependent.setJob(rs.getString("job"));
				dependent.setCompany(rs.getString("company"));
				dependent.setLivingTogether(rs.getBoolean("living_together"));
				dependent.setDependent(rs.getBoolean("dependent"));
				return dependent;
			}
		};
		String sql = "SELECT * FROM " + tableName + " WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void remove(String applicantId, String dependentName) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ? AND dependent_name = ?";
		template.executeUpdate(sql, applicantId, dependentName);
	}

}
