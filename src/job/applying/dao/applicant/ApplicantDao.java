package job.applying.dao.applicant;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.dao.common.CommonDao;
import job.applying.dao.education.EducationDao;
import job.applying.dao.jobObjective.JobObjectiveDao;
import job.applying.dao.user.UserDao;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.domain.user.User;

public class ApplicantDao implements CommonDao<Applicant, ApplicantNotFoundException> {

	@Override
	public void addOrUpdate(Applicant applicant) {
		JdbcTemplate template = new JdbcTemplate();
		Date birthDate = (applicant.getBirthDate() == null) ? null : Date.valueOf(applicant.getBirthDate());
		Date canJoinDate = (applicant.getCanJoinDate() == null) ? null : Date.valueOf(applicant.getCanJoinDate());

		String sql = "REPLACE INTO applicant_informations " + "(applicant_id, " + "applicant_email, "
				+ "applicant_korean_name, " + "applicant_chinese_name," + "applicant_birth_date," + "applicant_address,"
				+ "applicant_bonjuck," + "applicant_phone_number," + "applicant_cell_phone_number," + "whether_veteran,"
				+ "whether_disabled," + "can_join_date)" + "VALUES(?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicant.getUserId(), applicant.getEmail(), applicant.getKoreanName(),
				applicant.getChineseName(), birthDate, applicant.getAddress(), applicant.getBonjuck(),
				applicant.getPhoneNumber(), applicant.getCellPhoneNumber(), applicant.isWhetherVeteran(),
				applicant.isWhetherDisabled(), canJoinDate);
		
//		if (applicant.getJobObjective() != null) {
//			JobObjectiveDao jobObjectiveDao = new JobObjectiveDao();
//			jobObjectiveDao.addOrUpdate(applicant.getJobObjective(), applicant.getUserId());
//		}
//		
//		if (applicant.getEducations() != null) {
//			EducationDao educationDao = new EducationDao();
//			educationDao.addOrUpdates(applicant.getEducations(), applicant.getUserId());
//		}
	}

	@Override
	public Applicant findById(final String userId) {
		UserDao userDao = new UserDao();
		final User user = userDao.findById(userId);

		RowMapper<Applicant> rm = new RowMapper<Applicant>() {
			@Override
			public Applicant mapRow(ResultSet rs) throws SQLException {
				Applicant applicant = new Applicant(user);
				applicant.setEmail(rs.getString("applicant_email"));
				applicant.setKoreanName(rs.getString("applicant_korean_name"));
				applicant.setChineseName(rs.getString("applicant_chinese_name"));
				applicant.setBirthDate(LocalDate.parse(rs.getDate("applicant_birth_date").toString()));
				applicant.setAddress(rs.getString("applicant_address"));
				applicant.setBonjuck(rs.getString("applicant_bonjuck"));
				applicant.setPhoneNumber(rs.getString("applicant_phone_number"));
				applicant.setCellPhoneNumber(rs.getString("applicant_cell_phone_number"));
				applicant.setWhetherVeteran(rs.getBoolean("whether_veteran"));
				applicant.setWhetherDisabled(rs.getBoolean("whether_disabled"));
				applicant.setCanJoinDate(LocalDate.parse(rs.getDate("can_join_date").toString()));

//				if (rs.getString("job_objective_id") != null) {
//					JobObjective jobObjective = new JobObjective();
//					jobObjective.setApplyingField(rs.getString("applying_field"));
//					jobObjective.setAnnualSalary(rs.getInt("annual_salary"));
//					jobObjective.setDesiredAnnualSalary(rs.getInt("desired_annual_salary"));
//					applicant.setJobObjective(jobObjective);
//				} 
//				
//				EducationDao educationDao = new EducationDao();
//				applicant.setEducations(educationDao.findById(userId));
				return applicant;
			}
		};
		JdbcTemplate template = new JdbcTemplate();
		String sql = "SELECT A.applicant_id, " + "A.applicant_email, " + "A.applicant_korean_name, "
				+ "A.applicant_chinese_name," + "A.applicant_birth_date," + "A.applicant_address," + "A.applicant_bonjuck,"
				+ "A.applicant_phone_number," + "A.applicant_cell_phone_number," + "A.whether_veteran," + "A.whether_disabled,"
				+ "A.can_join_date, " + "B.applicant_id as job_objective_id , B.applying_field, B.annual_salary, B.desired_annual_salary "
				+ "FROM applicant_informations A " 
				+ "LEFT JOIN job_objectives B ON A.applicant_id = B.applicant_id "
				+ "WHERE A.applicant_id = ?";
		return template.executeQuery(sql, rm, userId);
	}

	@Override
	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM applicant_informations WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}
//
//	@Override
//	public void addOrUpdate(Applicant applicant) {
//		JdbcTemplate template = new JdbcTemplate();
//		Date birthDate = (applicant.getBirthDate() == null) ? null : Date.valueOf(applicant.getBirthDate());
//		Date canJoinDate = (applicant.getCanJoinDate() == null) ? null : Date.valueOf(applicant.getCanJoinDate());
//
//		String sql = "UPDATE applicant_informations SET " + "applicant_email=?, " + "applicant_korean_name=?, "
//				+ "applicant_chinese_name=?," + "applicant_birth_date=?," + "applicant_address=?," + "applicant_bonjuck=?,"
//				+ "applicant_phone_number=?," + "applicant_cell_phone_number=?," + "whether_veteran=?," + "whether_disabled=?,"
//				+ "can_join_date=?" + "WHERE applicant_id = ?";
//		template.executeUpdate(sql, applicant.getEmail(), applicant.getKoreanName(), applicant.getChineseName(), birthDate,
//				applicant.getAddress(), applicant.getBonjuck(), applicant.getPhoneNumber(), applicant.getCellPhoneNumber(),
//				applicant.isWhetherVeteran(), applicant.isWhetherDisabled(), canJoinDate, applicant.getUserId());
//	}

	@Override
	public List<Applicant> findAll() {
		return null;
	}

	public boolean hasRow(String applicantId) throws ApplicantNotFoundException {
		Applicant applicant = findById(applicantId);
		System.out.println(applicant);
		if (applicant == null) {
			throw new ApplicantNotFoundException();
		}
		return true;
	}

}
