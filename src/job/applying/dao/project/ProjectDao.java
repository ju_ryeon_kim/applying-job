package job.applying.dao.project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.project.Project;

public class ProjectDao {
	private String tableName = "projects";

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void remove(String applicantId, String key) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ? AND project = ?";
		template.executeUpdate(sql, applicantId, key);
	}

	public void addOrUpdates(List<Project> list, String applicantId) {
		for (Project item : list) {
			addOrUpdate(item, applicantId);
		}
	}

	private void addOrUpdate(Project item, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE " + tableName
				+ " (applicant_id, project, target_company, responsibilities, technology, start_date, end_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, item.getProject(), item.getTargetCompany(), item.getResponsibilities(),
				item.getTechnology(), item.getStartDate(), item.getEndDate());
	}

	public List<Project> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Project> rm = new RowMapper<Project>() {
			@Override
			public Project mapRow(ResultSet rs) throws SQLException {
				Project item = new Project();
				item.setProject(rs.getString("project"));
				item.setTargetCompany(rs.getString("target_company"));
				item.setResponsibilities(rs.getString("responsibilities"));
				item.setTechnology(rs.getString("technology"));
				item.setStartDate(rs.getDate("start_date").toLocalDate());
				item.setEndDate(rs.getDate("end_date").toLocalDate());
				return item;
			}
		};
		String sql = "SELECT * FROM " + tableName + " WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

}
