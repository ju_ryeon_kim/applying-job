package job.applying.dao.common;

import java.util.List;

public interface CommonDao<T, U extends Exception> {
	 void addOrUpdate(final T t);
	 T findById(final String id);
	 void remove(final String id);
//	 void update(final T t);
	 List<T> findAll();
	 boolean hasRow(String id) throws U;
}
