package job.applying.dao.computerSkill;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.dependent.Dependent;

public class ComputerSkillDao {
	private String tableName = "computer_skills";

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void addOrUpdates(List<ComputerSkill> list, String applicantId) {
		for (ComputerSkill item : list) {
			addOrUpdate(item, applicantId);
		}
	}

	private void addOrUpdate(ComputerSkill item, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE " + tableName
				+ " (applicant_id, computer_skill_type, tool, computer_skill_level, description) VALUES (?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, item.getType(), item.getTool(), item.getLevel(), item.getDescription());
	}

	public List<ComputerSkill> findListById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<ComputerSkill> rm = new RowMapper<ComputerSkill>() {
			@Override
			public ComputerSkill mapRow(ResultSet rs) throws SQLException {
				ComputerSkill item = new ComputerSkill();
				item.setTool(rs.getString("tool"));
				item.setType(rs.getString("computer_skill_type"));
				item.setLevel(rs.getString("computer_skill_level"));
				item.setDescription(rs.getString("description"));
				return item;
			}
		};
		String sql = "SELECT * FROM " + tableName + " WHERE applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void remove(String applicantId, String key) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ? AND tool = ?";
		template.executeUpdate(sql, applicantId, key);
	}

}
