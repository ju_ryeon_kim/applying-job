package job.applying.dao.selfIntroduction;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.selfIntroduction.SelfIntroduction;

public class SelfIntroductionDao {
	private String tableName = "self_Introductions";

	public int count(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Integer> rm = new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs) throws SQLException {
				return rs.getInt(1);
			}
		};
		String sql = "SELECT count(*) FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM " + tableName + " WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void addOrUpdate(SelfIntroduction item, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE " + tableName
				+ " (applicant_id, background, school_life, career_detail, strengths_weaknesses, motto_and_aim, purpose_and_dream) VALUES (?, ?, ?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, item.getBackground(), item.getSchoolLife(), item.getCareerDetail(),
				item.getStrengthsWeaknesses(), item.getMottoAndAim(), item.getPurposeAndDream());
	}

	public SelfIntroduction findById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<SelfIntroduction> rm = new RowMapper<SelfIntroduction>() {
			@Override
			public SelfIntroduction mapRow(ResultSet rs) throws SQLException {
				SelfIntroduction item = new SelfIntroduction();
				item.setBackground(rs.getString("background"));
				item.setSchoolLife(rs.getString("school_life"));
				item.setCareerDetail(rs.getString("career_detail"));
				item.setStrengthsWeaknesses(rs.getString("strengths_weaknesses"));
				item.setMottoAndAim(rs.getString("motto_and_aim"));
				item.setPurposeAndDream(rs.getString("purpose_and_dream"));
				return item;
			}
		};
		String sql = "SELECT * FROM " + tableName + " WHERE applicant_id = ?";
		return template.executeQuery(sql, rm, applicantId);
	}

}
