package job.applying.dao.language;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.language.Language;

public class LanguageDao {

	public List<Language> selectAll() {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Language> rm = new RowMapper<Language>() {
			@Override
			public Language mapRow(ResultSet rs) throws SQLException {
				Language language = new Language();
				language.setLanguageId(rs.getInt("language_id"));
				language.setLanguageName(rs.getString("language_name"));
				return language;
			}
		};
		String sql = "SELECT * FROM languages";
		return template.list(sql, rm);
	}

}
