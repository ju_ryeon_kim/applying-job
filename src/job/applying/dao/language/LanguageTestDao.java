package job.applying.dao.language;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.language.LanguageTest;

public class LanguageTestDao {

	public List<LanguageTest> findAll() {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<LanguageTest> rm = new RowMapper<LanguageTest>() {
			@Override
			public LanguageTest mapRow(ResultSet rs) throws SQLException {
				LanguageTest languageTest = new LanguageTest();
				languageTest.setLanguageId(rs.getInt("language_id"));
				languageTest.setLanguageName(rs.getString("language_name"));
				languageTest.setLanguageTestId(rs.getInt("language_test_id"));
				languageTest.setLanguageTestName(rs.getString("language_test_name"));
				return languageTest;
			}
		};
		String sql = "SELECT A.language_test_id, A.language_test_name, B.language_id, B.language_name FROM language_tests A "
				+ " JOIN languages B ON A.language_id = B.language_id";
		return template.list(sql, rm);
	}

	public void addOrUpdate(LanguageTest languageTest) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE language_tests (language_test_name, language_id) VALUES (?,?)";
		template.executeUpdate(sql, languageTest.getLanguageTestName(), languageTest.getLanguageId());
	}

	public LanguageTest findByName(String languageTestName) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<LanguageTest> rm = new RowMapper<LanguageTest>() {
			@Override
			public LanguageTest mapRow(ResultSet rs) throws SQLException {
				LanguageTest languageTest = new LanguageTest();
				languageTest.setLanguageId(rs.getInt("language_id"));
				languageTest.setLanguageName(rs.getString("language_name"));
				languageTest.setLanguageTestId(rs.getInt("language_test_id"));
				languageTest.setLanguageTestName(rs.getString("language_test_name"));
				return languageTest;
			}
		};
		String sql = "SELECT A.language_test_id, A.language_test_name, B.language_id, B.language_name FROM language_tests A "
				+ " JOIN languages B ON A.language_id = B.language_id WHERE A.language_test_name = ?";
		return template.executeQuery(sql, rm, languageTestName);
	}

}
