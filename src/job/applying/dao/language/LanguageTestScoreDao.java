package job.applying.dao.language;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.education.Education;
import job.applying.domain.education.EducationDetail;
import job.applying.domain.education.School;
import job.applying.domain.education.SchoolType;
import job.applying.domain.language.LanguageTest;
import job.applying.domain.language.LanguageTestScore;
import job.applying.service.language.LanguageService;
import job.applying.service.language.LanguageTestService;

public class LanguageTestScoreDao {

	public void addOrUpdate(LanguageTestScore languageTestScore, String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		if (LanguageService.isEtc(languageTestScore.getLanguageName())) {
			languageTestScore.setLanguageId(LanguageService.ETC_ID);
			LanguageTestService.save((LanguageTest) languageTestScore);
		}
		LanguageTest languageTest = LanguageTestService.get(languageTestScore.getLanguageTestName());

		String sql = "REPLACE language_test_scores "
				+ "(applicant_id, language_test_id, language_test_score, language_test_acquisition_date) "
				+ "VALUES (?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, languageTest.getLanguageTestId(), languageTestScore.getLanguageTestScore(),
				languageTestScore.getLanguageTestScoreAcquisitionDate());
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM language_test_scores WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public void addOrUpdates(List<LanguageTestScore> languagetestScores, String applicantId) {
		for (LanguageTestScore languageTestScore : languagetestScores) {
			addOrUpdate(languageTestScore, applicantId);
		}
	}

	public List<LanguageTestScore> findById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<LanguageTestScore> rm = new RowMapper<LanguageTestScore>() {
			@Override
			public LanguageTestScore mapRow(ResultSet rs) throws SQLException {
				LanguageTestScore languageTestScore = new LanguageTestScore();
				languageTestScore.setLanguageId(rs.getInt("language_id"));
				languageTestScore.setLanguageName(rs.getString("language_name"));
				languageTestScore.setLanguageTestId(rs.getInt("language_test_id"));
				languageTestScore.setLanguageTestName(rs.getString("language_test_name"));
				languageTestScore.setLanguageTestScore(rs.getFloat("language_test_score"));
				languageTestScore.setLanguageTestScoreAcquisitionDate(rs.getDate("language_test_acquisition_date").toLocalDate());
				return languageTestScore;
			}
		};
		String sql = " SELECT A.applicant_id, A.language_test_id, A.language_test_score, A.language_test_acquisition_date,"
				+ " B.language_test_id, B.language_test_name, "
				+ " C.language_id, C.language_name "
				+ " FROM language_test_scores A "
				+ " JOIN language_tests B ON A.language_test_id = B.language_test_id "
				+ " JOIN languages C ON B.language_id = C.language_id "
				+ " WHERE A.applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public void remove(String applicantId, int languageTestId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM language_test_scores WHERE applicant_id = ? AND language_test_id = ?";
		template.executeUpdate(sql, applicantId, languageTestId);
	}

}
