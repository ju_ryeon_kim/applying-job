package job.applying.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.dao.applicant.ApplicantNotFoundException;
import job.applying.dao.common.CommonDao;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.user.User;
import job.applying.domain.user.UserNotFoundException;

public class UserDao implements CommonDao<User, UserNotFoundException> {

	@Override
	public void addOrUpdate(User user) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE INTO users VALUES(?, ?)";
		template.executeUpdate(sql, user.getUserId(), user.getPassword());
	}

	@Override
	public User findById(String userId) {
		RowMapper<User> rm = new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs)  throws SQLException {
				return new User(rs.getString("user_id"), rs.getString("user_password"));
			}
		};
		JdbcTemplate template = new JdbcTemplate();
		String sql = "SELECT * FROM users WHERE user_id = ?";
		return template.executeQuery(sql, rm, userId);
	}

	@Override
	public void remove(String userId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM users WHERE user_id = ?";
		template.executeUpdate(sql, userId);
	}

//	@Override
//	public void addOrUpdate(User user) {
//		JdbcTemplate template = new JdbcTemplate();
//		String sql = "UPDATE users SET user_password=? WHERE user_id=?";
//		template.executeUpdate(sql, user.getPassword(), user.getUserId());
//	}

	@Override
	public List<User> findAll() {
		RowMapper<User> rm = new RowMapper<User>() {
			@Override
			public User mapRow(ResultSet rs)  throws SQLException {
				return new User(rs.getString("user_id"), rs.getString("user_password"));
			}
		};
		JdbcTemplate template = new JdbcTemplate();
		String sql = "SELECT * FROM users";
		return template.list(sql, rm);
	}

	@Override
	public boolean hasRow(String id) throws UserNotFoundException {
		User user = findById(id);
		if (user == null) {
			throw new UserNotFoundException();
		}
		return true;
	}

}
