package job.applying.dao.education;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.education.SchoolType;

public class SchoolTypeDao {

	public SchoolType findById(String schoolType) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<SchoolType> rm = new RowMapper<SchoolType>() {
			@Override
			public SchoolType mapRow(ResultSet rs) throws SQLException {
				SchoolType schoolType = new SchoolType(rs.getString("school_type_name"));
				return schoolType;
			}
		};
		String sql = "SELECT * FROM school_types WHERE school_type_name = ?";
		return template.executeQuery(sql, rm, schoolType);
	}

	public void add(SchoolType schoolType) {
		add(schoolType.getSchoolType());
	}

	public void remove(String schoolTypeName) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM school_types WHERE school_type_name = ?;";
		template.executeUpdate(sql, schoolTypeName);
	}

	public void add(String schoolType) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "INSERT INTO school_types (school_type_name) VALUES (?);";
		template.executeUpdate(sql, schoolType);
	}

	public List<SchoolType> findAll() {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<SchoolType> rm = new RowMapper<SchoolType>() {
			@Override
			public SchoolType mapRow(ResultSet rs) throws SQLException {
				return new SchoolType(rs.getString("school_type_name"));
			}
		};
		String sql = "SELECT * FROM school_types";
		return template.list(sql, rm);
	}
	
}
