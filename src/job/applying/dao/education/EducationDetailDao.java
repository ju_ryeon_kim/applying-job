package job.applying.dao.education;

import core.jdbc.JdbcTemplate;
import job.applying.domain.education.EducationDetail;

public class EducationDetailDao {
	public void addOrUpdate(EducationDetail detail, String applicantId, int schoolId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE education_details (applicant_id, school_id, education_major, education_credit, education_full_marks) VALUES (?, ?, ?, ?, ?)";
		template.executeUpdate(sql, applicantId, schoolId, detail.getEducationMajor(), detail.getEducationCredit(), detail.getEducationFullMarks());
	}
}
