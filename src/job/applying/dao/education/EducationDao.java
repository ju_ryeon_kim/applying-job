package job.applying.dao.education;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.dao.common.CommonDao;
import job.applying.domain.education.Education;
import job.applying.domain.education.EducationDetail;
import job.applying.domain.education.School;
import job.applying.domain.education.SchoolType;

public class EducationDao {


	public void addOrUpdate(Education education, String applicantId) {
		SchoolDao schoolDao = new SchoolDao();
		School school =  schoolDao.findById(education.getSchoolName(), education.getSchoolLocation());
		if (school == null) {
			schoolDao.add(new School(education.getSchoolName(), education.getSchoolLocation(), new SchoolType(education.getSchoolType())));
			school =  schoolDao.findById(education.getSchoolName(), education.getSchoolLocation());
		};
		int schoolId = school.getSchoolId();
		education.setSchoolId(schoolId);
		//학교 addOrUpdate
		Date educationStart = (education.getEducationStart() == null) ? null : Date.valueOf(education.getEducationStart());
		Date educationEnd = (education.getEducationEnd() == null) ? null : Date.valueOf(education.getEducationEnd());
		JdbcTemplate template = new JdbcTemplate();
		String sql = "REPLACE educations (applicant_id, school_id, education_start, education_end) VALUES (?, ?, ?, ?);";
		template.executeUpdate(sql, applicantId, education.getSchoolId(), education.getEducationStart(), education.getEducationEnd());
		if (education.getEducationDetail() != null) {
			EducationDetailDao educationDetailDao = new EducationDetailDao();
			educationDetailDao.addOrUpdate(education.getEducationDetail(), applicantId, schoolId);
		}
	}
	
	public void addOrUpdates(List<Education> educations, String applicantId) {
		for (Education education : educations) {
			addOrUpdate(education, applicantId);
		}
	}

	public List<Education> findById(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<Education> rm = new RowMapper<Education>() {
			@Override
			public Education mapRow(ResultSet rs) throws SQLException {
				String applicantId = rs.getString("applicant_id");
				School school = new School(new SchoolType(rs.getString("school_type_name")));
				school.setSchoolId(rs.getInt("school_id"));
				school.setSchoolName(rs.getString("school_name"));
				school.setSchoolLocation(rs.getString("school_location"));
				Education education = new Education(school);
				education.setEducationStart(LocalDate.parse(rs.getDate("education_start").toString()));
				education.setEducationEnd(LocalDate.parse(rs.getDate("education_end").toString()));
				String education_major = rs.getString("education_major");
				if (education_major != null) {
					EducationDetail educationDetail = new EducationDetail();
					educationDetail.setEducationMajor(education_major);
					educationDetail.setEducationCredit(rs.getFloat("education_credit"));
					educationDetail.setEducationFullMarks(rs.getFloat("education_full_marks"));
					education.setEducationDetail(educationDetail);
				}
			
				return education;
			}
		};
		String sql = " SELECT A.applicant_id, A.school_id, A.education_start, A.education_end,"
				+ " B.education_major, B.education_credit, B.education_full_marks, "
				+ " C.school_name, C.school_location, "
				+ " D.school_type_name "
				+ " FROM educations A "
				+ " LEFT JOIN education_details B ON A.applicant_id = B.applicant_id AND A.school_id = B.school_id "
				+ " JOIN schools C ON A.school_id = C.school_id "
				+ " JOIN school_types D ON C.school_type_name = D.school_type_name "
				+ " WHERE A.applicant_id = ?";
		return template.list(sql, rm, applicantId);
	}

	public void remove(String applicantId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM educations WHERE applicant_id = ?";
		template.executeUpdate(sql, applicantId);
	}

	public List<Education> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean hasRow(String id) throws EducationNotFoundException {
		// TODO Auto-generated method stub
		return false;
	}

	public void remove(String applicantId, int schoolId) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM educations WHERE applicant_id = ? AND school_id = ?";
		template.executeUpdate(sql, applicantId, schoolId);
	}

}
