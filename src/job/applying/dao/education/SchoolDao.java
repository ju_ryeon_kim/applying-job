package job.applying.dao.education;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.jdbc.JdbcTemplate;
import core.jdbc.RowMapper;
import job.applying.domain.education.School;
import job.applying.domain.education.SchoolType;

public class SchoolDao {

	public void add(School school) {
		SchoolTypeDao schoolTypeDao = new SchoolTypeDao();
		SchoolType schoolType = schoolTypeDao.findById(school.getSchoolType());
		if (schoolType == null) {
			schoolTypeDao.add(school.getSchoolType());
		}
		JdbcTemplate template = new JdbcTemplate();
		String sql = "INSERT INTO schools (school_name, school_location, school_type_name) VALUES (?, ?, ?);";
		template.executeUpdate(sql, school.getSchoolName(), school.getSchoolLocation(), school.getSchoolType());
	}


	public School findById(String schoolName, String schoolLocation) {
		JdbcTemplate template = new JdbcTemplate();
		RowMapper<School> rm = new RowMapper<School>() {
			@Override
			public School mapRow(ResultSet rs) throws SQLException {
				SchoolTypeDao schoolTypeDao = new SchoolTypeDao();
				SchoolType schoolType = schoolTypeDao.findById(rs.getString("school_type_name"));

				School school = new School(schoolType);
				school.setSchoolId(rs.getInt("school_id"));
				school.setSchoolName(rs.getString("school_name"));
				school.setSchoolLocation(rs.getString("school_location"));
				return school;
			}
		};
		String sql = "SELECT * FROM schools A JOIN school_types B ON A.school_type_name = B.school_type_name WHERE A.school_name = ? AND A.school_location = ?;";
		return template.executeQuery(sql, rm, schoolName, schoolLocation);
	}

	public void remove(String schoolName, String schoolLocation) {
		JdbcTemplate template = new JdbcTemplate();
		String sql = "DELETE FROM schools WHERE school_name = ? AND school_location = ?;";
		template.executeUpdate(sql, schoolName, schoolLocation);
		
	}

}
