package job.applying.web.applicant;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.user.User;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.user.UserService;

@WebServlet("/applicant/read")
public class ReadApplicantServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		
		Applicant applicant = ApplicantService.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(ApplicantService.REQUEST_APPLICANT, applicant);

		RequestDispatcher rd = req.getRequestDispatcher("/view/applicantInformation/read.jsp");
		rd.forward(req, resp);

	}
}
