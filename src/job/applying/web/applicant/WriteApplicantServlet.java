package job.applying.web.applicant;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.user.UserDao;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.user.User;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.user.UserService;

@WebServlet("/applicant/write")
public class WriteApplicantServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		HttpSession session = req.getSession();
		
		if (ApplicantService.exist(session)) {
			Applicant applicant = ApplicantService.get(SessionUtils.getStringValue(session, UserService.SESSION_USER_ID));
			req.setAttribute(ApplicantService.REQUEST_APPLICANT, applicant);
			rd = req.getRequestDispatcher("/view/applicantInformation/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/applicantInformation/create_form.jsp");
		}
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 로그인 유저 정보 생성
		HttpSession session = req.getSession();
		String userId = SessionUtils.getStringValue(session, UserService.SESSION_USER_ID);
		System.out.println("User id : " + userId);
		UserDao userDao = new UserDao();
		User user = userDao.findById(userId);

		// 기본인적사항
		String applicantKoreanName = req.getParameter("applicantKoreanName");
		String applicantChineseName = req.getParameter("applicantChineseName");
		LocalDate applicantBirthDate = LocalDate.parse(req.getParameter("applicantBirthDate"));
		String applicantAddress = req.getParameter("applicantAddress");
		String applicantBonjuck = req.getParameter("applicantBonjuck");
		String applicantPhoneNumber = req.getParameter("applicantPhoneNumber");
		String applicantCellPhoneNumber = req.getParameter("applicantCellPhoneNumber");
		String applicantEmail = req.getParameter("applicantEmail");
		boolean whetherVeteran = req.getParameter("whetherVeteran") == null ? false : true;
		boolean whetherDisabled = req.getParameter("whetherDisabled") == null ? false : true;
		System.out.println(req.getParameter("whetherDisabled"));
		System.out.println(req.getParameter("whetherVeteran"));
		LocalDate canJoinDate = LocalDate.parse(req.getParameter("canJoinDate"));

		Applicant applicant = new Applicant(user);
		applicant.setEmail(applicantEmail);
		applicant.setKoreanName(applicantKoreanName);
		applicant.setChineseName(applicantChineseName);
		applicant.setBirthDate(applicantBirthDate);
		applicant.setAddress(applicantAddress);
		applicant.setBonjuck(applicantBonjuck);
		applicant.setPhoneNumber(applicantPhoneNumber);
		applicant.setCellPhoneNumber(applicantCellPhoneNumber);
		applicant.setWhetherVeteran(whetherVeteran);
		applicant.setWhetherDisabled(whetherDisabled);
		applicant.setCanJoinDate(canJoinDate);

		ApplicantService.save(applicant);

		resp.sendRedirect("/applicant/read");
	}
}
