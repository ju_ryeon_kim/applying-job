package job.applying.web.applicant;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.applicant.Applicant;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.user.UserService;

@WebServlet("/applicant/delete")
public class DeleteApplicantServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		String userId = SessionUtils.getStringValue(session, UserService.SESSION_USER_ID);
		ApplicantService.delete(userId);
		session.removeAttribute(ApplicantService.SESSION_APPLICANT_ID);

		resp.sendRedirect("/");
	}
}
