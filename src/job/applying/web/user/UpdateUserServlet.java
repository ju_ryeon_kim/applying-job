package job.applying.web.user;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.dao.user.UserDao;
import job.applying.domain.user.User;
import job.applying.service.user.UserService;

@WebServlet("/user/update")
public class UpdateUserServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String userId = SessionUtils.getStringValue(session, UserService.SESSION_USER_ID);
		if (userId == null) {
			resp.sendRedirect("/");
			return;
		}

		System.out.println("User id : " + userId);
		UserDao userDao = new UserDao();
		User user = userDao.findById(userId);
		req.setAttribute("user", user);
		RequestDispatcher rd = req.getRequestDispatcher("/user_form.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String sessionUserId = SessionUtils.getStringValue(session, UserService.SESSION_USER_ID);
		if (sessionUserId == null) {
			resp.sendRedirect("/");
			return;
		}

		String userId = req.getParameter("userId");
		if (!sessionUserId.equals(userId)) {
			resp.sendRedirect("/");
			return;
		}

		String userPassword = req.getParameter("userPassword");
		User user = new User(userId, userPassword);
		UserDao userDao = new UserDao();
		userDao.addOrUpdate(user);
		resp.sendRedirect("/");
	}
}
