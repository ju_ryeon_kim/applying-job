package job.applying.web.user;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import job.applying.domain.user.PasswordMismatchException;
import job.applying.domain.user.UserNotFoundException;
import job.applying.service.user.UserService;

@WebServlet("/user/login")
public class LoginUserServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect("/login.jsp");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userId = req.getParameter("userId");
		String userPassword = req.getParameter("userPassword");
		
		try {
			UserService.login(userId, userPassword);
			HttpSession session = req.getSession();
			session.setAttribute(UserService.SESSION_USER_ID, userId);
			resp.sendRedirect("/");
			return;
		} catch (UserNotFoundException e) {
			//계정없음
			forwardLoginView(req, resp, "존재하지 않는 사용자입니다. 다시 로그인하세요");
			//파일로그를 남김
		} catch (PasswordMismatchException e) {
			forwardLoginView(req, resp, "비밀번호가 틀립니다. 다시 로그인하세요");
		} 
	}

	public void forwardLoginView(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
		req.setAttribute("errorMessage", errorMessage);
		RequestDispatcher rd = req.getRequestDispatcher("/login.jsp");
		rd.forward(req, resp);
//		resp.sendRedirect("/login.jsp?error=a");
	}
	
}
