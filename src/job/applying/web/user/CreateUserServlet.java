package job.applying.web.user;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import job.applying.dao.user.UserDao;
import job.applying.domain.user.User;

@WebServlet("/user/create")
public class CreateUserServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect("/user_form.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userId = req.getParameter("userId");
		String userPassword = req.getParameter("userPassword");

		UserDao userDao = new UserDao();
		userDao.addOrUpdate(new User(userId, userPassword));
		resp.sendRedirect("/");
	}
}
