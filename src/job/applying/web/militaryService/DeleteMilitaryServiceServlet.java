package job.applying.web.militaryService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.militaryService.MilitaryServiceService;

@WebServlet("/militaryService/delete")
public class DeleteMilitaryServiceServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		MilitaryServiceService mss = new MilitaryServiceService();
		mss.delete(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/applicant/read");
	}
}
