package job.applying.web.militaryService;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.militaryService.MilitaryService;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.militaryService.MilitaryServiceService;

@WebServlet("/militaryService/read")
public class ReadMilitaryServiceServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		MilitaryServiceService mss = new MilitaryServiceService();
		MilitaryService milstaryService = mss.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(mss.getRequestKey(), milstaryService);
		
		RequestDispatcher rd = req.getRequestDispatcher("/view/militaryService/read.jsp");
		rd.forward(req, resp);
	}
}
