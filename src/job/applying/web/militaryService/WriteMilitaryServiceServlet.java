package job.applying.web.militaryService;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.militaryService.MilitaryService;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.militaryService.MilitaryServiceService;

@WebServlet("/militaryService/write")
public class WriteMilitaryServiceServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		MilitaryServiceService mss = new MilitaryServiceService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (mss.exist(applicantId)) {
			req.setAttribute(mss.getRequestKey(), mss.get(applicantId));
			rd = req.getRequestDispatcher("/view/militaryService/update_form.jsp");
		} else {

			rd = req.getRequestDispatcher("/view/militaryService/create_form.jsp");
		}
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		MilitaryService militaryService = new MilitaryService();
		militaryService.setMilitaryServiceStatus(req.getParameter("militaryServiceStatus"));
		militaryService.setMilitaryServiceBranchName(req.getParameter("militaryServiceBranchName"));
		militaryService.setMilitaryDivisionName(req.getParameter("militaryDivisionName"));
		militaryService.setRankingName(req.getParameter("rankingName"));
		militaryService.setCompletionReason(req.getParameter("completionReason"));
		militaryService.setServicePeriod(Integer.parseInt(req.getParameter("servicePeriod")));
		militaryService.setExemptionReason(req.getParameter("exemptionReason"));

		MilitaryServiceService mss = new MilitaryServiceService();
		mss.save(militaryService, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/militaryService/read");
	}
}
