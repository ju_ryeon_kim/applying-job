package job.applying.web.language;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.education.Education;
import job.applying.domain.education.EducationDetail;
import job.applying.domain.education.School;
import job.applying.domain.education.SchoolType;
import job.applying.domain.language.Language;
import job.applying.domain.language.LanguageTest;
import job.applying.domain.language.LanguageTestScore;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.education.EducationService;
import job.applying.service.language.LanguageService;
import job.applying.service.language.LanguageTestScoreService;
import job.applying.service.language.LanguageTestService;

@WebServlet("/language/write")
public class WriteLanguageServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		RequestDispatcher rd = null;
		List<Language> languages = LanguageService.get();
		req.setAttribute(LanguageService.REQUEST_LANGUAGES, languages);

		List<LanguageTest> languageTests = LanguageTestService.getAll();
		req.setAttribute(LanguageTestService.REQUEST_LANGUAGE_TESTS, languageTests);

		if (LanguageTestScoreService.exist(session)) {
			req.setAttribute(LanguageTestScoreService.REQUEST_LANGUAGE_TEST_SCORES,
					LanguageTestScoreService.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID)));
			rd = req.getRequestDispatcher("/view/language/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/language/create_form.jsp");
		}
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 학력사항
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "languageTestScores");
		List<LanguageTestScore> languagetestScores = new ArrayList<>();
		for (Map<String, String> map : maps) {
			LanguageTestScore languageTestScore = new LanguageTestScore();
			languageTestScore.setLanguageTestScore(Float.parseFloat(map.get("languageTestScore")));
			languageTestScore
					.setLanguageTestScoreAcquisitionDate(LocalDate.parse(map.get("languageTestScoreAcquisitionDate")));
			languageTestScore.setLanguageName(map.get("language"));
			languageTestScore.setLanguageTestName(map.get("languageTest"));
			languagetestScores.add(languageTestScore);
		}

		HttpSession session = req.getSession();
		LanguageTestScoreService.save(languagetestScores,
				SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		session.setAttribute(LanguageTestScoreService.SESSION_LANGUAGE_TEST_SCORE_ID,
				ApplicantService.SESSION_APPLICANT_ID);

		resp.sendRedirect("/language/read");

	}
}
