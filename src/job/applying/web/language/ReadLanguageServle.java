package job.applying.web.language;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.language.LanguageTestScore;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.language.LanguageTestScoreService;

@WebServlet("/language/read")
public class ReadLanguageServle extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		List<LanguageTestScore> languageTestScores = LanguageTestScoreService.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(LanguageTestScoreService.REQUEST_LANGUAGE_TEST_SCORES, languageTestScores);
		RequestDispatcher rd = req.getRequestDispatcher("/view/language/read.jsp");
		rd.forward(req, resp);
	}
}
