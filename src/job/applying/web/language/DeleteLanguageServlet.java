package job.applying.web.language;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.education.EducationService;
import job.applying.service.language.LanguageTestScoreService;

@WebServlet({ "/language/delete", "/language/delete/*" })
public class DeleteLanguageServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String url = req.getRequestURI();
		String applicantId = SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID);
		session.removeAttribute(LanguageTestScoreService.SESSION_LANGUAGE_TEST_SCORE_ID);
		int languageTestId = 0;
		try {
			languageTestId = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1));
			LanguageTestScoreService.delete(applicantId, languageTestId);
			if (LanguageTestScoreService.exist(session)) {
				session.setAttribute(LanguageTestScoreService.SESSION_LANGUAGE_TEST_SCORE_ID, ApplicantService.SESSION_APPLICANT_ID);
				resp.sendRedirect("/language/read");
				return;
			}
		} catch (NumberFormatException e) {
			LanguageTestScoreService.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");
	}
}
