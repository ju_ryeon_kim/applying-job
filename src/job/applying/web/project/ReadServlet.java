package job.applying.web.project;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.project.Project;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.project.ProjectService;

@WebServlet("/project/read")
public class ReadServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ProjectService service = new ProjectService();
		List<Project> list = service
				.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(service.getRequestKey(), list);

		RequestDispatcher rd = req.getRequestDispatcher("/view/project/read.jsp");
		rd.forward(req, resp);
	}
}
