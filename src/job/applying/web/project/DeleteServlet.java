package job.applying.web.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.computerSkill.ComputerSkillService;
import job.applying.service.dependent.DependentService;
import job.applying.service.project.ProjectService;

@WebServlet({ "/project/delete", "/project/delete/*" })
public class DeleteServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String key = RequestUtils.parseLastPath(req);
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		ProjectService service = new ProjectService();
		if (!key.equals("delete")) {
			service.delete(applicantId, key);
			if (service.exist(applicantId)) {
				resp.sendRedirect("/project/read");
				return;
			}
		} else {
			service.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");
	}
}
