package job.applying.web.project;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.dependent.Dependent;
import job.applying.domain.project.Project;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.computerSkill.ComputerSkillService;
import job.applying.service.dependent.DependentService;
import job.applying.service.project.ProjectService;

@WebServlet("/project/write")
public class WriteServlet extends HttpServlet {
	private String moduleName = "project";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher rd = null;
		ProjectService service = new ProjectService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (service.exist(applicantId)) {
			List<Project> list = service.get(applicantId);
			req.setAttribute(service.getRequestKey(), list);
			rd = req.getRequestDispatcher("/view/" + moduleName + "/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/" + moduleName + "/create_form.jsp");
		}

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "projects");

		List<Project> list = new ArrayList<>();
		for (Map<String, String> m : maps) {
			Project item = new Project();
			item.setProject(m.get("project"));
			item.setTargetCompany(m.get("targetCompany"));
			item.setResponsibilities(m.get("responsibilities"));
			item.setTechnology(m.get("technology"));
			item.setStartDate(LocalDate.parse(m.get("startDate")));
			item.setEndDate(LocalDate.parse(m.get("endDate")));
			list.add(item);
		}

		ProjectService service = new ProjectService();
		service.save(list, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/" + moduleName + "/read");
	}

}
