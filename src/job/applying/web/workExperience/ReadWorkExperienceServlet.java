package job.applying.web.workExperience;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.workExperience.WorkExperience;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.workExperience.WorkExperienceService;

@WebServlet("/workExperience/read")
public class ReadWorkExperienceServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		WorkExperienceService wes = new WorkExperienceService();
		List<WorkExperience> workExps = wes.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		
		req.setAttribute(wes.getRequestKey(), workExps);
		
		RequestDispatcher rd = req.getRequestDispatcher("/view/workExperience/read.jsp");
		rd.forward(req, resp);
	}
}
