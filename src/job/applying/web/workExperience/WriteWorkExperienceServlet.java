package job.applying.web.workExperience;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.workExperience.WorkExperience;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.workExperience.WorkExperienceService;

@WebServlet("/workExperience/write")
public class WriteWorkExperienceServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		WorkExperienceService wes = new WorkExperienceService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (wes.exist(applicantId)) {
			List<WorkExperience> workExps = wes.get(applicantId);
			req.setAttribute(wes.getRequestKey(), workExps);
			rd = req.getRequestDispatcher("/view/workExperience/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/workExperience/create_form.jsp");
		}
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "workExperiences");

		List<WorkExperience> workExps = new ArrayList<>();
		for (Map<String, String> m : maps) {
			WorkExperience workExp = new WorkExperience();
			workExp.setCompanyName(m.get("companyName"));
			workExp.setDepartmentName(m.get("departmentName"));
			workExp.setWorkName(m.get("workName"));
			workExp.setPositionName(m.get("positionName"));
			workExp.setWorkStartDate(LocalDate.parse(m.get("workStartDate")));
			workExp.setWorkEndDate(LocalDate.parse(m.get("workEndDate")));
			workExp.setRetirementReason(m.get("retirementReason"));
			workExps.add(workExp);
		}

		WorkExperienceService ses = new WorkExperienceService();
		ses.save(workExps, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/workExperience/read");
	}
}
