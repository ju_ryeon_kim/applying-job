package job.applying.web.workExperience;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.workExperience.WorkExperienceService;

@WebServlet({"/workExperience/delete", "/workExperience/delete/*"})
public class DeleteWorkExperienceServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String companyName = RequestUtils.parseLastPath(req);
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		WorkExperienceService wes = new WorkExperienceService();
		if (!companyName.equals("delete")){
			wes.delete(applicantId, companyName);
			if (wes.exist(applicantId)) {
				resp.sendRedirect("/workExperience/read");
				return;
			}
		} else {
			wes.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");
	}
}
