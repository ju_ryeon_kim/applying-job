package job.applying.web.computerSkill;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.computerSkill.ComputerSkillService;

@WebServlet("/computerSkill/read")
public class ReadtServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ComputerSkillService service = new ComputerSkillService();
		List<ComputerSkill> list = service
				.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(service.getRequestKey(), list);

		RequestDispatcher rd = req.getRequestDispatcher("/view/computerSkill/read.jsp");
		rd.forward(req, resp);
	}
}
