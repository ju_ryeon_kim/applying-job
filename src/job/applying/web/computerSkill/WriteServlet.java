package job.applying.web.computerSkill;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.dependent.Dependent;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.computerSkill.ComputerSkillService;
import job.applying.service.dependent.DependentService;

@WebServlet("/computerSkill/write")
public class WriteServlet extends HttpServlet {
	private String moduleName = "computerSkill";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher rd = null;
		ComputerSkillService service = new ComputerSkillService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (service.exist(applicantId)) {
			List<ComputerSkill> dependents = service.get(applicantId);
			req.setAttribute(service.getRequestKey(), dependents);
			rd = req.getRequestDispatcher("/view/" + moduleName + "/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/" + moduleName + "/create_form.jsp");
		}

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "computerSkills");

		List<ComputerSkill> list = new ArrayList<>();
		for (Map<String, String> m : maps) {
			ComputerSkill item = new ComputerSkill();
			item.setTool(m.get("tool"));
			item.setType(m.get("type"));
			item.setLevel(m.get("level"));
			item.setDescription(m.get("description"));
			list.add(item);
		}

		ComputerSkillService service = new ComputerSkillService();
		service.save(list, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/" + moduleName + "/read");
	}

}
