package job.applying.web.applyingRoute;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.applyingRoute.ApplyingRoute;
import job.applying.domain.applyingRoute.ApplyingRouteType;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.applyingRoute.ApplyingRouteService;
import job.applying.service.applyingRoute.ApplyingRouteTypeService;
import job.applying.service.computerSkill.ComputerSkillService;

@WebServlet("/applyingRoute/write")
public class WriteServlet extends HttpServlet {
	private String moduleName = "applyingRoute";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher rd = null;
		ApplyingRouteService service = new ApplyingRouteService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (service.exist(applicantId)) {
			List<ApplyingRoute> list = service.get(applicantId);
			req.setAttribute(service.getRequestKey(), list);
			rd = req.getRequestDispatcher("/view/" + moduleName + "/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/" + moduleName + "/create_form.jsp");
		}

		ApplyingRouteTypeService artService = new ApplyingRouteTypeService();
		List<ApplyingRouteType> listType = artService.get();
		req.setAttribute(artService.getRequestKey(), listType);

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "applyingRoutes");

		List<ApplyingRoute> list = new ArrayList<>();
		for (Map<String, String> m : maps) {
			ApplyingRoute item = new ApplyingRoute();
			item.setId(Integer.parseInt(m.get("applyingRouteTypeId")));
			item.setApplyingRoute(m.get("applyingRoute"));
			list.add(item);
		}

		ApplyingRouteService service = new ApplyingRouteService();
		service.save(list, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/" + moduleName + "/read");
	}

}
