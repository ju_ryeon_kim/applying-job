package job.applying.web.applyingRoute;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.applyingRoute.ApplyingRoute;
import job.applying.domain.applyingRoute.ApplyingRouteType;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.applyingRoute.ApplyingRouteService;
import job.applying.service.applyingRoute.ApplyingRouteTypeService;

@WebServlet("/applyingRoute/read")
public class ReadtServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ApplyingRouteService arService = new ApplyingRouteService();
		List<ApplyingRoute> list = arService
				.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(arService.getRequestKey(), list);
		

		RequestDispatcher rd = req.getRequestDispatcher("/view/applyingRoute/read.jsp");
		rd.forward(req, resp);
	}
}
