package job.applying.web.applyingRoute;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.applyingRoute.ApplyingRouteService;

@WebServlet({ "/applyingRoute/delete", "/applyingRoute/delete/*" })
public class DeleteServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String key = RequestUtils.parseLastPath(req);
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		ApplyingRouteService service = new ApplyingRouteService();
		if (!key.equals("delete")) {
			service.delete(applicantId, key);
			if (service.exist(applicantId)) {
				resp.sendRedirect("/applyingRoute/read");
				return;
			}
		} else {
			service.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");
	}
}
