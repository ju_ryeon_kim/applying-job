package job.applying.web.certificate;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.certificate.Certificate;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.certificate.CertificateService;

@WebServlet("/certificate/write")
public class WriteCertificateServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		HttpSession session = req.getSession();
		String applicantId = SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID);
		CertificateService certificateService = new CertificateService();
		if (certificateService.exist(applicantId)) {
			req.setAttribute(CertificateService.REQUEST_CERTIFICATES, CertificateService.getList(applicantId));
			rd = req.getRequestDispatcher("/view/certificate/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/certificate/create_form.jsp");
		}
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "certificates");

		List<Certificate> certificates = new ArrayList<>();
		for (Map<String, String> map : maps) {
			Certificate certificate = new Certificate();
			certificate.setCertificateName(map.get("certificateName"));
			certificate.setCertificateAuthority(map.get("certificateAuthority"));
			certificate.setCertificateAcquisitionDate(LocalDate.parse(map.get("certificateAcquisitionDate")));
			certificates.add(certificate);
		}

		CertificateService.save(certificates, SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/certificate/read");
	}
}
