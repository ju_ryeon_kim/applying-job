package job.applying.web.certificate;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.certificate.Certificate;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.certificate.CertificateService;

@WebServlet("/certificate/read")
public class ReadCertificateServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		List<Certificate> certificates = CertificateService.getList(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(CertificateService.REQUEST_CERTIFICATES, certificates);
		
		RequestDispatcher rd = req.getRequestDispatcher("/view/certificate/read.jsp");
		rd.forward(req, resp);
	}
}
