package job.applying.web.certificate;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.certificate.CertificateService;

@WebServlet({ "/certificate/delete", "/certificate/delete/*" })
public class DeleteCertificateServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String certificateName = RequestUtils.parseLastPath(req);
		System.out.println(URLDecoder.decode(certificateName, "utf-8"));
		String applicantId = SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID);
		if (!certificateName.equals("delete")) {
			CertificateService.delete(applicantId, certificateName);
			CertificateService certificateService = new CertificateService();
			if (certificateService.exist(applicantId)) {
				resp.sendRedirect("/certificate/read");
				return;
			}
		} else {
			CertificateService.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");
	}
}
