package job.applying.web.education;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.education.Education;
import job.applying.domain.education.EducationDetail;
import job.applying.domain.education.School;
import job.applying.domain.education.SchoolType;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.education.EducationService;
import job.applying.service.education.SchoolTypeService;
import job.applying.service.jobObjective.JobObjectiveService;

@WebServlet("/education/write")
public class WriteEducationServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		HttpSession session = req.getSession();
		if (EducationService.exist(session)) {
			req.setAttribute(EducationService.REQUEST_EDUCATIONS,
					EducationService.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID)));
			rd = req.getRequestDispatcher("/view/education/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/education/create_form.jsp");
		}
		req.setAttribute(SchoolTypeService.REQUEST_SCHOOL_TYPES, SchoolTypeService.getSchoolTypes());
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 학력사항
		String educationsVal = req.getParameter("educations");
		String[] educationsArr = educationsVal.split(";");
		List<Education> educations = new ArrayList<>();
		List<Map<String, String>> maps = new ArrayList<>();
		for (int i = 0; i < educationsArr.length - 1; i++) {
			StringTokenizer st = new StringTokenizer(educationsArr[i], ",");
			Map<String, String> map = new HashMap<>();
			while (st.hasMoreTokens()) {
				String keyValue = st.nextToken();
				int delimIndex = keyValue.indexOf(':');
				if (delimIndex >= 0) {
					String key = keyValue.substring(0, delimIndex).trim();
					String value = keyValue.substring(delimIndex + 1).trim();
					map.put(key, value);
				}
			}
			maps.add(map);
		}

		for (Map<String, String> map : maps) {
			School school = new School(map.get("schoolName"), map.get("schoolLocation"),
					new SchoolType(map.get("schoolType")));
			Education education = new Education(school);
			education.setEducationStart(LocalDate.parse(map.get("educationStart")));
			education.setEducationEnd(LocalDate.parse(map.get("educationEnd")));
			String educationMajor = map.get("educationMajor");
			if (educationMajor != null && !educationMajor.equals("")) {
				EducationDetail detail = new EducationDetail();
				detail.setEducationMajor(educationMajor);
				Float educationCredit = map.get("educationCredit") == null || map.get("educationCredit").equals("") ? null
						: Float.parseFloat(map.get("educationCredit"));
				Float educationFullMarks = map.get("educationFullMarks") == null || map.get("educationFullMarks").equals("")
						? null : Float.parseFloat(map.get("educationFullMarks"));
				detail.setEducationCredit(educationCredit);
				detail.setEducationFullMarks(educationFullMarks);
				education.setEducationDetail(detail);
			}
			educations.add(education);
		}
		
		HttpSession session = req.getSession();
		EducationService.save(educations, SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		session.setAttribute(EducationService.SESSION_EDUCATION_ID, ApplicantService.SESSION_APPLICANT_ID);
		
		
		resp.sendRedirect("/education/read");
	}
}
