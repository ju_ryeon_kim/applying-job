package job.applying.web.education;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.education.Education;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.education.EducationService;

@WebServlet({ "/education/delete", "/education/delete/*" })
public class DeleteEducationServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String url = req.getRequestURI();
		String applicantId = SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID);
		session.removeAttribute(EducationService.SESSION_EDUCATION_ID);
		int schoolId = 0;
		try {
			schoolId = Integer.parseInt(url.substring(url.lastIndexOf("/") + 1));
			EducationService.delete(applicantId, schoolId);
			if (EducationService.exist(session)) {
				session.setAttribute(EducationService.SESSION_EDUCATION_ID, ApplicantService.SESSION_APPLICANT_ID);
				resp.sendRedirect("/education/read");
				return;
			}
		} catch (NumberFormatException e) {
			EducationService.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");

	}
}
