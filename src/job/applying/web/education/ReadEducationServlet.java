package job.applying.web.education;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.education.Education;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.education.EducationService;
import job.applying.service.jobObjective.JobObjectiveService;

@WebServlet("/education/read")
public class ReadEducationServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		List<Education> educations = EducationService.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		System.out.println(educations.get(0).getSchoolId());
		req.setAttribute(EducationService.REQUEST_EDUCATIONS, educations);
		RequestDispatcher rd = req.getRequestDispatcher("/view/education/read.jsp");
		rd.forward(req, resp);
	}
}
