package job.applying.web.selfIntroduction;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.careerStatement.CareerStatement;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.dependent.Dependent;
import job.applying.domain.project.Project;
import job.applying.domain.selfIntroduction.SelfIntroduction;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.careerStatement.CareerStatementService;
import job.applying.service.computerSkill.ComputerSkillService;
import job.applying.service.dependent.DependentService;
import job.applying.service.project.ProjectService;
import job.applying.service.selfIntroduction.SelfIntroductionService;

@WebServlet("/selfIntroduction/write")
public class WriteServlet extends HttpServlet {
	private String moduleName = "selfIntroduction";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher rd = null;
		SelfIntroductionService service = new SelfIntroductionService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (service.exist(applicantId)) {
			SelfIntroduction item  = service.get(applicantId);
			req.setAttribute(service.getRequestKey(), item);
			rd = req.getRequestDispatcher("/view/" + moduleName + "/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/" + moduleName + "/create_form.jsp");
		}

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		SelfIntroduction item = new SelfIntroduction();
		item.setBackground(req.getParameter("background"));
		item.setSchoolLife(req.getParameter("schoolLife"));
		item.setCareerDetail(req.getParameter("careerDetail"));
		item.setStrengthsWeaknesses(req.getParameter("strengthsWeaknesses"));
		item.setMottoAndAim(req.getParameter("mottoAndAim"));
		item.setPurposeAndDream(req.getParameter("purposeAndDream"));

		SelfIntroductionService service = new SelfIntroductionService();
		service.save(item, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/" + moduleName + "/read");
	}

}
