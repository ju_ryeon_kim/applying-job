package job.applying.web.selfIntroduction;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.careerStatement.CareerStatement;
import job.applying.domain.selfIntroduction.SelfIntroduction;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.careerStatement.CareerStatementService;
import job.applying.service.selfIntroduction.SelfIntroductionService;

@WebServlet("/selfIntroduction/read")
public class ReadServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		SelfIntroductionService service = new SelfIntroductionService();
		SelfIntroduction item = service
				.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(service.getRequestKey(), item);

		RequestDispatcher rd = req.getRequestDispatcher("/view/selfIntroduction/read.jsp");
		rd.forward(req, resp);
	}
}
