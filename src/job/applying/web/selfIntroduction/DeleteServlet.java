package job.applying.web.selfIntroduction;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.selfIntroduction.SelfIntroductionService;

@WebServlet({ "/selfIntroduction/delete" })
public class DeleteServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String key = RequestUtils.parseLastPath(req);
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		SelfIntroductionService service = new SelfIntroductionService();

		service.delete(applicantId);

		resp.sendRedirect("/applicant/read");
	}
}
