package job.applying.web.careerStatement;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.careerStatement.CareerStatement;
import job.applying.domain.computerSkill.ComputerSkill;
import job.applying.domain.dependent.Dependent;
import job.applying.domain.project.Project;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.careerStatement.CareerStatementService;
import job.applying.service.computerSkill.ComputerSkillService;
import job.applying.service.dependent.DependentService;
import job.applying.service.project.ProjectService;

@WebServlet("/careerStatement/write")
public class WriteServlet extends HttpServlet {
	private String moduleName = "careerStatement";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher rd = null;
		CareerStatementService service = new CareerStatementService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (service.exist(applicantId)) {
			List<CareerStatement> list = service.get(applicantId);
			req.setAttribute(service.getRequestKey(), list);
			rd = req.getRequestDispatcher("/view/" + moduleName + "/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/" + moduleName + "/create_form.jsp");
		}

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "careerStatements");

		List<CareerStatement> list = new ArrayList<>();
		for (Map<String, String> m : maps) {
			CareerStatement item = new CareerStatement();
			item.setCompany(m.get("company"));
			item.setDepartment(m.get("department"));
			item.setPosition(m.get("position"));
			item.setCompanyStatus(m.get("companyStatus"));
			item.setProjectStartDate(LocalDate.parse(m.get("projectStartDate")));
			item.setProjectEndDate(LocalDate.parse(m.get("projectEndDate")));
			item.setMainBusiness(m.get("mainBusiness"));
			item.setWorkStartDate(LocalDate.parse(m.get("workStartDate")));
			item.setWorkEndDate(LocalDate.parse(m.get("workEndDate")));
			item.setMainRole(m.get("mainRole"));
			item.setBusinessPerformance(m.get("businessPerformance"));
			list.add(item);
		}

		CareerStatementService service = new CareerStatementService();
		service.save(list, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/" + moduleName + "/read");
	}

}
