package job.applying.web.jobObjective;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.jobObjective.JobObjectiveService;
import job.applying.service.user.UserService;

@WebServlet("/jobObjective/delete")
public class DeleteJobObjectiveServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		JobObjectiveService.delete(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		session.removeAttribute(JobObjectiveService.SESSION_JOB_OBJECTIVE_ID);
		
		resp.sendRedirect("/applicant/read");
	}
}
