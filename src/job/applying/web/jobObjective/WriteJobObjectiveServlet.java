package job.applying.web.jobObjective;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.jobObjective.JobObjectiveService;

@WebServlet("/jobObjective/write")
public class WriteJobObjectiveServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		HttpSession session = req.getSession();
		if (JobObjectiveService.exist(session)) {
			JobObjective jobObejctive = JobObjectiveService
					.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
			req.setAttribute(JobObjectiveService.REQUEST_JOB_OBJECTIVE, jobObejctive);

			rd = req.getRequestDispatcher("/view/jobObjective/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/jobObjective/create_form.jsp");
		}
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 기본지원사항
		String applyingField = req.getParameter("applyingField");
		int annualSalary = Integer.parseInt(req.getParameter("annualSalary"));
		int desiredAnnualSalary = Integer.parseInt(req.getParameter("desiredAnnualSalary"));

		JobObjective jobObjective = new JobObjective();
		jobObjective.setApplyingField(applyingField);
		jobObjective.setAnnualSalary(annualSalary);
		jobObjective.setDesiredAnnualSalary(desiredAnnualSalary);

		HttpSession session = req.getSession();
		JobObjectiveService.save(jobObjective, SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		session.setAttribute(JobObjectiveService.SESSION_JOB_OBJECTIVE_ID, ApplicantService.SESSION_APPLICANT_ID);

		resp.sendRedirect("/jobObjective/read");
	}
}
