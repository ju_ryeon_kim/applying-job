package job.applying.web.jobObjective;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import core.SessionUtils;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.jobObjective.JobObjectiveService;

@WebServlet("/jobObjective/read")
public class ReadJobObjectiveServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		JobObjective jobObjective = JobObjectiveService.get(SessionUtils.getStringValue(session, ApplicantService.SESSION_APPLICANT_ID));
		System.out.println(jobObjective);
		req.setAttribute(JobObjectiveService.REQUEST_JOB_OBJECTIVE, jobObjective);
		
		RequestDispatcher rd = req.getRequestDispatcher("/view/jobObjective/read.jsp");
		rd.forward(req, resp);
	}
}
