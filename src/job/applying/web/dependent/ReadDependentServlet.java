package job.applying.web.dependent;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.dependent.Dependent;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.dependent.DependentService;

@WebServlet("/dependent/read")
public class ReadDependentServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		DependentService service = new DependentService();
		List<Dependent> list = service
				.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(service.getRequestKey(), list);

		RequestDispatcher rd = req.getRequestDispatcher("/view/dependent/read.jsp");
		rd.forward(req, resp);
	}
}
