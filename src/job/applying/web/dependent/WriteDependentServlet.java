package job.applying.web.dependent;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.dependent.Dependent;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.dependent.DependentService;

@WebServlet("/dependent/write")
public class WriteDependentServlet extends HttpServlet {
	private String moduleName = "dependent";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		RequestDispatcher rd = null;
		DependentService service = new DependentService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (service.exist(applicantId)) {
			List<Dependent> dependents = service.get(applicantId);
			req.setAttribute(service.getRequestKey(), dependents);
			rd = req.getRequestDispatcher("/view/" + moduleName + "/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/" + moduleName + "/create_form.jsp");
		}

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "dependents");

		List<Dependent> dependents = new ArrayList<>();
		for (Map<String, String> m : maps) {
			Dependent dependent = new Dependent();
			dependent.setDependentName(m.get("dependentName"));
			dependent.setRelationShip(m.get("relationShip"));
			dependent.setBirthDate(LocalDate.parse(m.get("birthDate")));
			dependent.setEducation(m.get("education"));
			dependent.setJob(m.get("job"));
			dependent.setCompany(m.get("company"));
			dependent.setLivingTogether(m.get("livingTogeter") == null ? false : true);
			dependent.setDependent(m.get("dependent") == null ? false : true);
			dependents.add(dependent);
		}

		DependentService service = new DependentService();
		service.save(dependents, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/" + moduleName + "/read");
	}

}
