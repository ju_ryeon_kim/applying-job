package job.applying.web.dependent;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;
import job.applying.service.dependent.DependentService;

@WebServlet({"/dependent/delete", "/dependent/delete/*"})
public class DeleteDependentServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String dependentName = RequestUtils.parseLastPath(req);
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		DependentService service = new DependentService();
		if (!dependentName.equals("delete")){
			service.delete(applicantId, dependentName);
			if (service.exist(applicantId)) {
				resp.sendRedirect("/dependent/read");
				return;
			}
		} else {
			service.delete(applicantId);
		}
		resp.sendRedirect("/applicant/read");
	}
}
