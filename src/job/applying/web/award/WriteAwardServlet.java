package job.applying.web.award;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.RequestUtils;
import core.SessionUtils;
import job.applying.domain.award.Award;
import job.applying.domain.workExperience.WorkExperience;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;

@WebServlet("/award/write")
public class WriteAwardServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = null;
		AwardService as = new AwardService();
		String applicantId = SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID);
		if (as.exist(applicantId)) {
			List<Award> awards = as.get(applicantId);
			req.setAttribute(as.getRequestKey(), awards);
			rd = req.getRequestDispatcher("/view/award/update_form.jsp");
		} else {
			rd = req.getRequestDispatcher("/view/award/create_form.jsp");
		}

		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, String>> maps = RequestUtils.translateParams(req, "awards");

		List<Award> awards = new ArrayList<>();
		for (Map<String, String> m : maps) {
			Award award = new Award();
			award.setAwardName(m.get("awardName"));
			award.setAwardGrade(m.get("awardGrade"));
			award.setAwardAuthority(m.get("awardAuthority"));
			award.setAwardAcquisitionDate(LocalDate.parse(m.get("awardAcquisitionDate")));
			awards.add(award);
		}

		AwardService as = new AwardService();
		as.save(awards, SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));

		resp.sendRedirect("/award/read");
	}
}
