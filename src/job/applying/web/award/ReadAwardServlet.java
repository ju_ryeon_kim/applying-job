package job.applying.web.award;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import core.SessionUtils;
import job.applying.domain.award.Award;
import job.applying.service.applicant.ApplicantService;
import job.applying.service.award.AwardService;

@WebServlet("/award/read")
public class ReadAwardServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		AwardService as = new AwardService();
		List<Award> awards = as.get(SessionUtils.getStringValue(req.getSession(), ApplicantService.SESSION_APPLICANT_ID));
		req.setAttribute(as.getRequestKey(), awards);
		
		RequestDispatcher rd = req.getRequestDispatcher("/view/award/read.jsp");
		rd.forward(req, resp);
	}
}
