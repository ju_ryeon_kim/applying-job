package job.applying.service.user;

import static org.junit.Assert.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.dao.user.UserDao;
import job.applying.domain.user.PasswordMismatchException;
import job.applying.domain.user.User;
import job.applying.domain.user.UserNotFoundException;
import job.applying.domain.user.UserTest;

public class UserServiceTest {
	private UserDao userDao;
	@Before
	public void setup() throws Exception{
		userDao = new UserDao();
	}
	
	@After
	public void tearDown() throws Exception {
		userDao.remove(UserTest.TEST_USER.getUserId());
	}
	
	@Test
	public void login() throws Exception {
		User user = UserTest.TEST_USER;
		userDao.addOrUpdate(user);
		assertTrue(UserService.login(user.getUserId(), user.getPassword()));
	}
	
	@Test
	public void Join() throws Exception {
		User user = UserTest.TEST_USER;
		UserService.join(user);
		assertTrue(UserService.login(user));
	}
	
	@Test(expected=UserIdAlreadyInUseException.class)
	public void JoinWhenUserIdAleadyInUse() throws Exception {
		User user = UserTest.TEST_USER;
		UserService.join(user);
		UserService.join(user);
	}
	
	@Test(expected=UserNotFoundException.class)
	public void loginWhenNotExistedUser() throws Exception {
		User user = UserTest.TEST_USER;
		userDao.addOrUpdate(user);
		UserService.login("userId2", user.getPassword());
	}
	
	@Test(expected=PasswordMismatchException.class)
	public void loginWhenPasswordMismatch() throws Exception {
		User user = UserTest.TEST_USER;
		userDao.addOrUpdate(user);
		UserService.login(user.getUserId(), "password2");
	}
}
