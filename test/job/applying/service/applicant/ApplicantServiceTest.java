package job.applying.service.applicant;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.dao.applicant.ApplicantDao;
import job.applying.dao.user.UserDao;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.applicant.ApplicantTest;
import job.applying.domain.education.Education;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.domain.user.UserTest;
import job.applying.service.user.UserService;

public class ApplicantServiceTest {

	private UserDao userDao;
	private ApplicantDao applicantDao;

	@Before
	public void setUp() throws Exception {
		userDao = new UserDao();
		applicantDao = new ApplicantDao();
	}

	@After
	public void tearDown() throws Exception {
		reset();
	}

	private void reset() {
		userDao.remove(UserTest.TEST_USER.getUserId());
	}

	private void init() {
		reset();
		userDao.addOrUpdate(UserTest.TEST_USER);
		ApplicantService.save(ApplicantTest.TEST_APPLICANT);
	}

	@Test
	public void saveApplicant() throws Exception{
		// Given
		reset();
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		userDao.addOrUpdate(applicant);

		// when
		ApplicantService.save(applicant);

		// then
		Applicant dbApplicant = ApplicantService.get(applicant);
		assertEquals(applicant, dbApplicant);
	}

	@Test
	public void applicant얻어오기When필드값이다있음() throws Exception {
		// Given
		init();
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		// when
		Applicant dbApplicant = ApplicantService.get(applicant);
		// then
		assertEquals(applicant, dbApplicant);
	}

	@Test
	public void applicant얻어오기When의존성객체없음() throws Exception {
		// Given
		reset();
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		userDao.addOrUpdate(applicant);
		applicant.setJobObjective(null);
		applicantDao.addOrUpdate(applicant);
		// when
		Applicant dbApplicant = ApplicantService.get(applicant);
		// then
		assertEquals(applicant, dbApplicant);
	}
	
	@Test
	public void applicant수정하기() {
		init();
		Applicant dbApplicant1 = ApplicantService.get(ApplicantTest.TEST_APPLICANT);
		dbApplicant1.getJobObjective().setAnnualSalary(0);
		ApplicantService.save(ApplicantTest.TEST_APPLICANT);
		Applicant dbApplicant2 = ApplicantService.get(dbApplicant1);
		assertNotEquals(dbApplicant1, dbApplicant2);
	}
	
	@Test
	public void applicant삭제하기() throws Exception {
		//Given
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		UserService.join(applicant);

		// when
		ApplicantService.save(applicant);
		
		//when
		ApplicantService.delete(applicant.getUserId());
		Applicant dbApplicant = ApplicantService.get(applicant);
		//then
		assertNull(dbApplicant);
	}
	
	@Test
	public void jobObject얻어오기() throws Exception {
		//Given
		init();

		//when
		Applicant applicant =	ApplicantService.get(ApplicantTest.TEST_APPLICANT);
		JobObjective jobObjective = applicant.getJobObjective();
		//then
		assertNotNull(jobObjective);
	}

	@Test
	public void education얻어오기() throws Exception{
			init();
			Applicant applicant =	ApplicantService.get(ApplicantTest.TEST_APPLICANT);
			List<Education> educations = applicant.getEducations();
			assertTrue(educations.size() > 0);
	}
}
