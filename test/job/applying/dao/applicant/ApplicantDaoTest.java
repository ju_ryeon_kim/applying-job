package job.applying.dao.applicant;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.dao.user.UserDao;
import job.applying.domain.applicant.Applicant;
import job.applying.domain.applicant.ApplicantTest;
import job.applying.domain.jobObjective.JobObjectiveTest;
import job.applying.domain.user.UserTest;

public class ApplicantDaoTest {
	private UserDao userDao;
	private ApplicantDao applicantDao;

	@Before
	public void setup() throws Exception {
		userDao = new UserDao();
		applicantDao = new ApplicantDao();
		init();
	}

	@After
	public void teadDown() throws Exception {
		reset();
	}

	public void init() {
		userDao.addOrUpdate(UserTest.TEST_USER);
	}

	public void reset() {
		applicantDao.remove(ApplicantTest.TEST_APPLICANT.getUserId());
		userDao.remove(UserTest.TEST_USER.getUserId());
	}

	@Test
	public void applicant생성() throws Exception {
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		applicant.setJobObjective(JobObjectiveTest.TEST_JOB_OBJECTIVE);
		applicantDao.addOrUpdate(applicant);

		Applicant dbApplicant = applicantDao.findById(applicant.getUserId());
		assertNotNull(dbApplicant.getJobObjective());
		
		
//		Applicant dbApplicant = applicantDao.findById(applicant.getUserId());
//		assertEquals(applicant, dbApplicant);
//		//
//		applicant.setKoreanName("hunt");
//		applicantDao.update(applicant);
//		dbApplicant = applicantDao.findById(applicant.getUserId());
//		assertEquals(applicant, dbApplicant);
//
//		applicantDao.remove(applicant.getUserId());
//		dbApplicant = applicantDao.findById(applicant.getUserId());
//		assertNull(dbApplicant);
	}

	@Test
	public void applicant삭제() throws Exception {
	}

	@Test
	public void applicant읽기() throws Exception {
	}

	@Test
	public void applicant수정() throws Exception {
	}

	@Test
	public void hasRow() throws Exception {
		// Given
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		// when
		applicantDao.addOrUpdate(applicant);
		// then
		assertTrue(applicantDao.hasRow(applicant.getUserId()));
	}

	@Test(expected = ApplicantNotFoundException.class)
	public void hasRowWhenNotExistedApplicant() throws Exception {
		// Given
		Applicant applicant = ApplicantTest.TEST_APPLICANT;
		// when
		applicantDao.addOrUpdate(applicant);
		// then
		assertTrue(applicantDao.hasRow("honga"));
	}

}
