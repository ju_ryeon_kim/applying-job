package job.applying.dao.jobObjective;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.dao.applicant.ApplicantDao;
import job.applying.dao.user.UserDao;
import job.applying.domain.applicant.ApplicantTest;
import job.applying.domain.jobObjective.JobObjective;
import job.applying.domain.jobObjective.JobObjectiveTest;
import job.applying.domain.user.UserTest;

public class JobObjectiveDaoTest {
	private UserDao userDao;
	private ApplicantDao applicantDao;
	private JobObjectiveDao jobObjectiveDao;

	@Before
	public void setup() throws Exception {
		userDao = new UserDao();
		applicantDao = new ApplicantDao();
		jobObjectiveDao = new JobObjectiveDao();
	}

	@After
	public void teadDown() throws Exception {
		reset();
	}

	public void init() {
		userDao.addOrUpdate(UserTest.TEST_USER);
		applicantDao.addOrUpdate(ApplicantTest.TEST_APPLICANT);
	}

	public void reset() {
		userDao.remove(UserTest.TEST_USER.getUserId());
	}

	@Test
	public void cr() throws Exception {
		init();
		JobObjective jobObjective = JobObjectiveTest.TEST_JOB_OBJECTIVE;
		// Given
		jobObjectiveDao.addOrUpdate(jobObjective, ApplicantTest.TEST_APPLICANT.getUserId());
		JobObjective dbJobObjective = jobObjectiveDao.findById(ApplicantTest.TEST_APPLICANT.getUserId());
		assertEquals(jobObjective, dbJobObjective);
		// when

		// then
	}
	

}
