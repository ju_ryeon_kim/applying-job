package job.applying.dao.education;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.dao.applicant.ApplicantDao;
import job.applying.dao.user.UserDao;
import job.applying.domain.applicant.ApplicantTest;
import job.applying.domain.education.Education;
import job.applying.domain.education.EducationTest;
import job.applying.domain.education.School;
import job.applying.domain.education.SchoolTest;
import job.applying.domain.user.UserTest;
import job.applying.service.applicant.ApplicantService;

public class EducationDaoTest {
	private UserDao userDao;
	private ApplicantDao applicantDao;
	private EducationDao educationDao;
	private SchoolDao schoolDao;
	private SchoolTypeDao schoolTypeDao;

	@Before
	public void setup() throws Exception {
		userDao = new UserDao();
		applicantDao = new ApplicantDao();
		educationDao = new EducationDao();
		schoolDao = new SchoolDao();
		schoolTypeDao = new SchoolTypeDao();
	}

	@After
	public void teadDown() throws Exception {
		reset();
	}
	

	public void init() {
		ApplicantService.save(ApplicantTest.TEST_APPLICANT);
		schoolDao.add(SchoolTest.TEST_SCHOOL);
	}

	public void reset() {
		ApplicantService.delete(ApplicantTest.TEST_APPLICANT.getUserId());
		Education eduction = EducationTest.TEST_EDUCATIONS.get(0);
		School school = SchoolTest.TEST_SCHOOL;
		educationDao.remove(ApplicantTest.TEST_APPLICANT.getUserId());
		schoolDao.remove(eduction.getSchoolName(), eduction.getSchoolLocation());
		schoolTypeDao.remove(eduction.getSchoolType());
	}

	@Test
	public void cr() throws Exception {
		init();
		List<Education> educations = EducationTest.TEST_EDUCATIONS;
		// Given
		educationDao.addOrUpdates(educations, ApplicantTest.TEST_APPLICANT.getUserId());
		List<Education> dbEducations = educationDao.findById(ApplicantTest.TEST_APPLICANT.getUserId());
		assertEquals(educations.size(), dbEducations.size());
		// when

		// then
	}
	
}
