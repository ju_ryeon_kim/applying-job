package job.applying.dao.education;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.domain.education.School;
import job.applying.domain.education.SchoolTest;

public class SchoolDaoTest {
	private SchoolDao schoolDao;

	@Before
	public void setup() throws Exception {
		schoolDao = new  SchoolDao();
	}

	@After
	public void teadDown() throws Exception {
		School school = SchoolTest.TEST_SCHOOL;
		schoolDao.remove(school.getSchoolName(), school.getSchoolLocation());
	}
	
	@Test
	public void crd() throws Exception {
		//Given
		School school = SchoolTest.TEST_SCHOOL;
		schoolDao.add(school);

		//when
		School dbSchool = schoolDao.findById(school.getSchoolName(), school.getSchoolLocation());
		school.setSchoolId(dbSchool.getSchoolId());
		//then
		assertEquals(school, dbSchool);
	}
}
