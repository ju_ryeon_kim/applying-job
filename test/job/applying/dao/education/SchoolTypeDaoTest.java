package job.applying.dao.education;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import job.applying.dao.applicant.ApplicantDao;
import job.applying.dao.education.SchoolTypeDao;
import job.applying.dao.user.UserDao;
import job.applying.domain.education.SchoolType;
import job.applying.domain.education.SchoolTypeTest;

public class SchoolTypeDaoTest {
	SchoolTypeDao schoolTypeDao = new SchoolTypeDao();

	@Before
	public void setup() throws Exception {
	}

	@After
	public void teadDown() throws Exception {
		schoolTypeDao.remove(SchoolTypeTest.TEST_SCHOOL_TYPE.getSchoolType());
	}
	
	
	@Test
	public void crd() throws Exception {
		// Given
		SchoolType schoolType = SchoolTypeTest.TEST_SCHOOL_TYPE;
		// when
		schoolTypeDao.remove(SchoolTypeTest.TEST_SCHOOL_TYPE.getSchoolType());
		schoolTypeDao.add(schoolType);
		SchoolType dbSchoolType = schoolTypeDao.findById(schoolType.getSchoolType());

		// then
		assertEquals(schoolType, dbSchoolType);
	}
}
