package job.applying.dao.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import job.applying.dao.user.UserDao;
import job.applying.domain.user.User;
import job.applying.domain.user.UserTest;

public class UserDAOTest {
	private UserDao userDao;

	@Before
	public void setup() {
		userDao = new UserDao();
	}
	
	public void tearDown() {
		User user = UserTest.TEST_USER;
		userDao.remove(user.getUserId());
	}

	@Test
	public void crud() throws Exception {
		User user = UserTest.TEST_USER;
		userDao.remove(user.getUserId());
		userDao.addOrUpdate(user);

		User dbUser = userDao.findById(user.getUserId());
		assertEquals(UserTest.TEST_USER, dbUser);
		
		User updateUser = new User(user.getUserId(), "updatePassword");
		userDao.addOrUpdate(updateUser);
		dbUser = userDao.findById(user.getUserId());
		assertEquals(updateUser, dbUser);
	}

	@Test
	public void 존재하지_않는_사용자_조회() throws Exception {
		User user = UserTest.TEST_USER;
		userDao.remove(user.getUserId());
		User dbUser = userDao.findById(user.getUserId());
		assertNull(dbUser);
	}

	@Test
	public void findUsers() throws Exception {
		//when
		List<User> users = userDao.findAll();
		//then
		assertTrue(users.size() > 0);
	}
}
