package job.applying.domain.education;

import static org.junit.Assert.*;

import org.junit.Test;

public class SchoolTest {
	public static School TEST_SCHOOL = new School(SchoolTypeTest.TEST_SCHOOL_TYPE);

	static {
		TEST_SCHOOL.setSchoolName("황지초등학교");
	  TEST_SCHOOL.setSchoolLocation("강원도 태백시");
	}
}
