package job.applying.domain.education;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class EducationTest {

	public static List<Education> TEST_EDUCATIONS = new ArrayList<>();	
	
	static{
		Education education = new Education(SchoolTest.TEST_SCHOOL);
		education.setEducationStart(LocalDate.parse("1992-03-02"));
		education.setEducationEnd(LocalDate.parse("1998-02-28"));
		TEST_EDUCATIONS.add(education);
	}
	
}
