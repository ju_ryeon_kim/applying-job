package job.applying.domain.jobObjective;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import job.applying.domain.applicant.ApplicantTest;

public class JobObjectiveTest {
	public static JobObjective TEST_JOB_OBJECTIVE = new JobObjective();
	static {
		TEST_JOB_OBJECTIVE.setApplyingField("개발");
		TEST_JOB_OBJECTIVE.setAnnualSalary(2400);
		TEST_JOB_OBJECTIVE.setDesiredAnnualSalary(2400);
	}
	
	@Test
	public void 생성() throws Exception {
		JobObjective jobObjectvie = TEST_JOB_OBJECTIVE;
		assertNotNull(jobObjectvie);
	}
}
