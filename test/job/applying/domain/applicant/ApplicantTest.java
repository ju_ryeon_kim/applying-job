package job.applying.domain.applicant;

import java.time.LocalDate;

import job.applying.domain.education.EducationTest;
import job.applying.domain.jobObjective.JobObjectiveTest;
import job.applying.domain.user.UserTest;

public class ApplicantTest {
	public static Applicant TEST_APPLICANT = new Applicant(UserTest.TEST_USER);

	static {
		TEST_APPLICANT.setKoreanName("김주련");
		TEST_APPLICANT.setChineseName("金周鍊");
		TEST_APPLICANT.setBirthDate(LocalDate.parse("1985-05-28"));
		TEST_APPLICANT.setAddress("강원도 춘천시 소양로2가");
		TEST_APPLICANT.setBonjuck("강원도 삼척시");
		TEST_APPLICANT.setPhoneNumber("");
		TEST_APPLICANT.setCellPhoneNumber("010-6376-1147");
		TEST_APPLICANT.setEmail("frotime24@gmail.com");
		TEST_APPLICANT.setWhetherVeteran(false);
		TEST_APPLICANT.setWhetherDisabled(false);
		TEST_APPLICANT.setCanJoinDate(LocalDate.parse("2016-07-01"));
		
		TEST_APPLICANT.setJobObjective(JobObjectiveTest.TEST_JOB_OBJECTIVE);
		TEST_APPLICANT.setEducations(EducationTest.TEST_EDUCATIONS);
	}
	

}
