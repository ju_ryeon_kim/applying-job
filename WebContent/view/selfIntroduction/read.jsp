<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>자기소개서</h1>
	<p>성장과정: <p>${selfIntroduction.background }</p></p>
	<p>학교생활: <p>${selfIntroduction.schoolLife }</p></p>
	<p>세부경력사항: <p>${selfIntroduction.careerDetail }</p></p>
	<p>성격의 장단점: <p>${selfIntroduction.strengthsWeaknesses }</p></p>
	<p>좌우명 & 삶의 목표: <p>${selfIntroduction.mottoAndAim }</p></p>
	<p>지원동기 및 입사 후 포부: <p>${selfIntroduction.purposeAndDream }</p></p>
	<hr>
	<a href="/selfIntroduction/write">[수정]</a>
	<a href="/selfIntroduction/delete">[삭제]</a>
</body>
</html>