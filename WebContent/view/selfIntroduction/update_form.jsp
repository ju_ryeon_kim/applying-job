<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>자기소개서</h1>
	<form action="/selfIntroduction/write" method="post">
		<p>
			성장과정: <textarea name="background" rows="6" cols="100">${selfIntroduction.background }</textarea></p>
		<p>
			학교생활: <textarea name="schoolLife" rows="6" cols="100">${selfIntroduction.schoolLife }</textarea></p>
		<p>
			세부경력사항: <textarea name="careerDetail" rows="6" cols="100">${selfIntroduction.careerDetail }</textarea></p>
		<p>
			성격의 장단점: <textarea name="strengthsWeaknesses" rows="6" cols="100">${selfIntroduction.strengthsWeaknesses }</textarea></p>
		<p>
			좌우명 & 삶의 목표: <textarea name="mottoAndAim" rows="6" cols="100">${selfIntroduction.mottoAndAim }</textarea></p>
		<p>
			지원동기 및 입사 후 포부: <textarea name="purposeAndDream" rows="6" cols="100">${selfIntroduction.purposeAndDream }</textarea></p>
		<hr>
		<input type="submit" value="수정하기" />
	</form>
</body>
</html>