<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>학력사항</h1>
	<form action="/education/write" method="post">
		<div id="educationDiv">
			<input type="hidden" id="educations" name="educations" />
			<div id="educationTemplate" style="display: none">
				<p>
					구분: <select class="schoolType">
						<c:forEach var="schoolType" items="${schoolTypes }">
							<option value="${schoolType.schoolType }">${schoolType.schoolType }</option>
						</c:forEach>
					</select>
				</p>
				<p>
					학교명 : <input type="text" class="schoolName" value="황지고" />
				</p>
				<p>
					기간: <input type="text" class="educationStart" value="2004-03-02" /><input
						type="text" class="educationEnd" value="2008-02-28" />
				</p>
				<p>
					소재지: <input type="text" class="schoolLocation" value="강원도 태백" />
				</p>
				<p>
					전공: <input type="text" class="educationMajor" value="교육" />
				</p>
				<p>
					취득: <input type="text" class="educationCredit" value="3.4" />
				</p>
				<p>
					만점: <input type="text" class="educationFullMarks" value="4.3" />
				</p>
				<input class="bTnDeleteEducation" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAddEducation" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="등록하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var addEducation = function addEducation(e) {
			var educationTemplate = document
					.querySelector("#educationTemplate");
			var newEducation = educationTemplate.cloneNode(true);
			newEducation.id = "";
			newEducation.className = "education";
			newEducation.style.display = "block";
			newEducation.dataset.index = document
					.querySelectorAll(".education").length;
			educationTemplate.parentNode.insertBefore(newEducation,
					educationTemplate);
		};

		var deleteEducation = function(e) {
			if (e.target.className != "bTnDeleteEducation") {
				return;
			}
			var educationEl = e.target.closest(".education");
			educationEl.parentNode.removeChild(educationEl);
		};

		document.querySelector("#educationDiv").addEventListener("click",
				deleteEducation, false);
		document.querySelector("#bTnAddEducation").addEventListener("click",
				addEducation, false);
		addEducation();

		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var educations = [];
							var educationEls = document
									.querySelectorAll(".education");
							for ( var index in educationEls) {
								if (!educationEls.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = educationEls[index];
								debugger;
								var education = "schoolType : "
										+ tempEl.querySelector(".schoolType").value
										+ ", "
										+ "schoolName: "
										+ tempEl.querySelector(".schoolName").value
										+ ", "
										+ "educationStart: "
										+ tempEl
												.querySelector(".educationStart").value
										+ ", "
										+ "educationEnd: "
										+ tempEl.querySelector(".educationEnd").value
										+ ", "
										+ "educationMajor: "
										+ tempEl
												.querySelector(".educationMajor").value
										+ ", "
										+ "schoolLocation: "
										+ tempEl
												.querySelector(".schoolLocation").value
										+ ", "
										+ "educationCredit: "
										+ tempEl
												.querySelector(".educationCredit").value
										+ ", "
										+ "educationFullMarks: "
										+ tempEl
												.querySelector(".educationFullMarks").value
										+ "; ";
								educations.push(education);
							}
							document.querySelector("#educations").value = educations
									.join("");
						}, false);

	}
</script>

</html>