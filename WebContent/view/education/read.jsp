<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>학력사항</h1>
	<c:forEach var="education" items="${ educations }">
		<p>구분: ${education.schoolType }</p>
		<p>학교명 : ${education.schoolName }</p>
		<p>기간: ${education.educationStart }-${education.educationEnd }</p>
		<p>소재지: ${education.schoolLocation }</p>
		<c:if test="${not empty education.educationDetail }">
			<p>전공: ${education.educationDetail.educationMajor }</p>
			<p>취득: ${education.educationDetail.educationCredit }</p>
			<p>만점: ${education.educationDetail.educationFullMarks }</p>
		</c:if>
		<a href="/education/delete/${education.schoolId }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/education/write">[수정]</a><a href="/education/delete">[삭제]</a>
</body>
</html>