<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>수상경력</h1>
	<c:forEach var="dependent" items="${ dependents }">
			<p>
					관계: ${dependent.relationShip}
				</p>
				<p>
					성명: ${dependent.dependentName}
				</p>
				<p>
					생년월일: ${dependent.birthDate}
				</p>
				<p>
					학력: ${dependent.education}
				</p>
				<p>
					직업: ${dependent.job}
				</p>
				<p>
					직장명: ${dependent.company}
				</p>
				<p>
					동거여부: ${dependent.livingTogether}
				</p>
				<p>
					부양여부: ${dependent.dependent}
				</p>
		<p>
			<a href="/dependent/delete/${dependent.dependentName }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/dependent/write">[수정]</a>
	<a href="/dependent/delete">[삭제]</a>
</body>
</html>