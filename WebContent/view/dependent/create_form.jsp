<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>가족사항</h1>
	<form action="/dependent/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="dependents" name="dependents" />
			<div id="template" style="display: none">
				<p>
					관계: <input type="text" class="relationShip" value="부" />
				</p>
				<p>
					성명: <input type="text" class="dependentName" value="아버지" />
				</p>
				<p>
					생년월일: <input type="text" class="birthDate" value="1999-01-01" />
				</p>
				<p>
					학력: <input type="text" class="education" value="고졸" />
				</p>
				<p>
					직업: <input type="text" class="job" value="드라이버" />
				</p>
				<p>
					직장명: <input type="text" class="company" value="" />
				</p>
				<p>
					동거여부: <input type="checkbox" class="livingTogether" />
				</p>
				<p>
					부양여부: <input type="checkbox" class="dependent" checked />
				</p>
				<p>
					<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="등록하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "dependents";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);

		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		add();
		document.querySelector("input[type=submit]").addEventListener(
				"click",
				function(e) {
					//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
					var array = [];
					var els = document.querySelectorAll("." + className);
					for ( var index in els) {
						if (!els.hasOwnProperty(index)) {
							continue;
						}
						var tempEl = els[index];
						var str = "relationShip : "
								+ tempEl.querySelector(".relationShip").value
								+ ", " + "dependentName: "
								+ tempEl.querySelector(".dependentName").value
								+ ", " + "birthDate: "
								+ tempEl.querySelector(".birthDate").value
								+ ", " + "education: "
								+ tempEl.querySelector(".education").value
								+ ", " + "job: "
								+ tempEl.querySelector(".job").value + ", "
								+ "company: "
								+ tempEl.querySelector(".company").value + ", "
								+ "livingTogether: "
								+ tempEl.querySelector(".livingTogether").value
								+ ", " + "dependent: "
								+ tempEl.querySelector(".dependent").value
								+ "; ";
						array.push(str);

					}

					document.querySelector("#dependents").value = array
							.join("");

				}, false);

	}
</script>
</body>
</html>