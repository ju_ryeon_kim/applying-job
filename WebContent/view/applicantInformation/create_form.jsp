<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>기본인적사항</h1>
	<form action="/applicant/write" method="post">
<!-- 		<p> -->
<!-- 			사진: <img /> -->
<!-- 		</p> -->
		<p>
			성명(한글): <input type="text" name="applicantKoreanName" value="김주련" />
		</p>
		<p>
			성명(한자): <input type="text" name="applicantChineseName" value="金周鍊" />
		</p>
		<p>
			생년월일: <input type="text" name="applicantBirthDate" value="1985-05-28" />
		</p>
		<p>
			현주소: <input type="text" name="applicantAddress" value="강원도 춘천시" />
		</p>
		<p>
			본적: <input type="text" name="applicantBonjuck" value="강원도 삼척시" />
		</p>
		<p>
			전화번호: <input type="text" name="applicantPhoneNumber" value="" />
		</p>
		<p>
			휴대폰: <input type="text" name="applicantCellPhoneNumber"
				value="010-6376-1147" />
		</p>
		<p>
			이메일: <input type="text" name="applicantEmail"
				value="frotime24@gmail.com" />
		</p>
		<p>
			보훈여부: <input type="checkbox" name="whetherVeteran" />
		</p>
		<p>
			장애인여부: <input type="checkbox" name="whetherDisabled" />
		</p>
		<p>
			입사시기: <input type="text" name="canJoinDate" value="2016-07-01" />
		</p>
		<hr>
		<input type="submit" value="등록하기" />
	</form>
</body>
</html>