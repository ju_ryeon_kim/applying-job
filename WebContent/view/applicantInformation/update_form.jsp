<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>기본인적사항</h1>
<form action="/applicant/write" method="post">
<!-- <p> -->
<!-- 사진: <img/> -->
<!-- </p> -->
<p>
성명(한글): <input type="text" name="applicantKoreanName" value="${ applicant.koreanName }"/>
</p>
<p>
성명(한자): <input type="text" name="applicantChineseName" value="${ applicant.chineseName }"/>
</p>
<p>
생년월일: <input type="text" name="applicantBirthDate" value="${ applicant.birthDate }"/>
</p>
<p>
현주소: <input type="text" name="applicantAddress" value="${ applicant.address }"/>
</p>
<p>
본적: <input type="text" name="applicantBonjuck" value="${ applicant.bonjuck }"/>
</p>
<p>
전화번호: <input type="text" name="applicantPhoneNumber" value="${ applicant.phoneNumber }"/>
</p>
<p>
휴대폰: <input type="text" name="applicantCellPhoneNumber" value="${ applicant.cellPhoneNumber }"/>
</p>
<p>
이메일: <input type="text" name="applicantEmail" value="${ applicant.email }"/>
</p>
<p>
보훈여부: <input type="checkbox" name="whetherVeteran" ${ applicant.whetherVeteran == true ? "checked" : ""  } />
</p>
<p>
장애인여부: <input type="checkbox" name="whetherDisabled" ${ applicant.whetherDisabled  == true ? "checked" : "" } />
</p>
<p>
입사시기: <input type="text" name="canJoinDate" value="${ applicant.canJoinDate }"/>
</p>
<hr>
<input type="submit" value="수정하기"/>
</form>
</body>
</html>