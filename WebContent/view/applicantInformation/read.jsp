<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>기본인적사항</h1>
<!-- 	<p> -->
<!-- 		사진: <img /> -->
<!-- 	</p> -->
	<p>성명(한글): ${ applicant.koreanName }</p>
	<p>성명(한자): ${ applicant.chineseName }</p>
	<p>생년월일: ${ applicant.birthDate }</p>
	<p>현주소: ${ applicant.address }</p>
	<p>본적: ${ applicant.bonjuck }</p>
	<p>전화번호: ${ applicant.phoneNumber }</p>
	<p>휴대폰: ${ applicant.cellPhoneNumber }</p>
	<p>이메일: ${ applicant.email }</p>
	<p>보훈여부: ${ applicant.whetherVeteran }</p>
	<p>장애인여부: ${ applicant.whetherDisabled }</p>
	<p>입사시기: ${ applicant.canJoinDate }</p>
	<hr>
		<a href="/applicant/write">[수정]</a>
</body>
</html>