<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>IT활용능력</h1>
	<c:forEach var="computerSkill" items="${ computerSkills }">
		<p>
			구분: ${computerSkill.type }
		</p>
		<p>
			사용프로그램: ${computerSkill.tool }
		</p>
		<p>
			등급: ${computerSkill.level }
		</p>
		<p>
			상세설명: ${computerSkill.description }
		</p>
		<p>
			<a href="/computerSkill/delete/${computerSkill.tool }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/computerSkill/write">[수정]</a>
	<a href="/computerSkill/delete">[삭제]</a>
</body>
</html>