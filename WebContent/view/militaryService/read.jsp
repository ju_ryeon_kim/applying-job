<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>병역사항</h1>
		<p>
			군필여부: ${militaryService.militaryServiceStatus }
		</p>
		<p>
			군별: ${militaryService.militaryServiceBranchName }
		</p>
		<p>
			병과: ${militaryService.militaryDivisionName }
		</p>
		<p>
			계급:  ${militaryService.rankingName }
		</p>
		<p>
			전역사유:  ${militaryService.completionReason }
		</p>
		<p>
			복무기간(개월):  ${militaryService.servicePeriod }
		</p>
		<p>
			면제사유:  ${militaryService.exemptionReason }
		</p>
		<hr>
	<hr>
	<a href="/militaryService/write">[수정]</a><a href="/militaryService/delete">[삭제]</a>
</body>
</html>