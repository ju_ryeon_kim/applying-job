<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>병역사항</h1>
	<form action="/militaryService/write" method="post">
		<p>
			군필여부: <input type="text" name="militaryServiceStatus" value="전역" />
		</p>
		<p>
			군별: <input type="text" name="militaryServiceBranchName" value="육군" />
		</p>
		<p>
			병과: <input type="text" name="militaryDivisionName" value="보병" />
		</p>
		<p>
			계급: <input type="text" name="rankingName" value="중위" />
		</p>
		<p>
			전역사유: <input type="text" name="completionReason" value="만기" />
		</p>
		<p>
			복무기간(개월): <input type="text" name="servicePeriod" value="28" />
		</p>
		<p>
			면제사유: <input type="text" name="exemptionReason" value="" />
		</p>
		<hr>
		<input type="submit" value="등록하기" />
	</form>
</body>
</html>