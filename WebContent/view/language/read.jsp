<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>외국어사항</h1>
	<c:forEach var="languageTestScore" items="${ languageTestScores }">
		<p>언어: ${languageTestScore.languageName }</p>
		<p>시험종류: ${languageTestScore.languageTestName }
		<p>점수 : ${languageTestScore.languageTestScore }</p>
		<p>취득일자: ${languageTestScore.languageTestScoreAcquisitionDate }</p>
			<a href="/language/delete/${languageTestScore.languageTestId }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/language/write">[수정]</a>
	<a href="/language/delete">[삭제]</a>
</body>
</html>