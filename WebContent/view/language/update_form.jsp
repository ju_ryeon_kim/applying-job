<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>외국어사항</h1>
	<form action="/language/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="languageTestScores"
				name="languageTestScores" />
			<c:if test="${not empty languageTestScores }">
				<c:forEach items="${languageTestScores }" var="languageTestScore">
					<div class="languageTestScores">
						<p>
							언어: <select class="language">
								<c:forEach var="language" items="${languages }">
									<option value="${language.languageName }"
										${ language.languageName == languageTestScore.languageName ? "selected" : "" }>${language.languageName }</option>
								</c:forEach>
							</select>
						</p>
						<p>
							시험종류: <select
								class="languageTest ${ languageTestScore.languageId == 4 ? "hidden" : "" }">
								<c:forEach var="languageTest" items="${languageTests }">
									<option data-language-name="${ languageTest.languageName }"
										value="${languageTest.languageTestName }"
										${ languageTest.languageTestName == languageTestScore.languageTestName ? "selected" : "" }>${languageTest.languageTestName }</option>
								</c:forEach>
							</select> <input type="text"
								class="languageTest ${ languageTestScore.languageId != 4 ? "
								hidden" : "" }" value="${ languageTestScore.languageTestName }" />
						</p>
						<p>
							점수 : <input type="text" class="languageTestScore"
								value="${ languageTestScore.languageTestScore}" />
						</p>
						<p>
							취득일자: <input type="text" class="languageTestScoreAcquisitionDate"
								value="${ languageTestScore.languageTestScoreAcquisitionDate}" />
						</p>
						<input class="bTnDelete" type="button" value="삭제하기">
						<hr>
					</div>
				</c:forEach>
			</c:if>
			<div id="template" style="display: none">
				<p>
					언어: <select class="language">
						<c:forEach var="language" items="${languages }">
							<option value="${language.languageName }">${language.languageName }</option>
						</c:forEach>
					</select>
				</p>
				<p>

					시험종류: <select class="languageTest">
						<c:forEach var="languageTest" items="${languageTests }">
							<option data-language-name="${ languageTest.languageName }"
								value="${languageTest.languageTestName }">${languageTest.languageTestName }</option>
						</c:forEach>
					</select> <input type="text" class="languageTest hidden" value="운전면허" />
				</p>
				<p>
					점수 : <input type="text" class="languageTestScore" value="700" />
				</p>
				<p>
					취득일자: <input type="text" class="languageTestScoreAcquisitionDate"
						value="2004-03-02" />
				</p>
				<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "languageTestScores";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		var change = function(e) {
			if (e.target.className != "language") {
				return;
			}
			var cateSelectEl = e.target;
			var cateName = cateSelectEl.value;
			var parentEl = cateSelectEl.closest('.' + className);
			var subSelectEl = parentEl.querySelector('select.languageTest');
			var subInputEl = parentEl.querySelector('input.languageTest');
			for ( var index in subSelectEl) {
				if (!subSelectEl.hasOwnProperty(index)) {
					continue;
				}
				var option = subSelectEl[index];
				option.className = "";
				if (cateName != option.dataset.languageName) {
					option.className = "hidden";
				}
				if (cateName != subSelectEl.selectedOptions[0].dataset.languageName) {
					subSelectEl.selectedIndex = index;
				}
			}

			subSelectEl.className = "languageTest hidden";
			subInputEl.className = "languageTest hidden";
			if (cateName == "기타") {
				subInputEl.className = "languageTest";
			} else {
				subSelectEl.className = "languageTest";
			}
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);
		document.querySelector("#createDiv").addEventListener("click", change,
				false);
		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								var str = "language : "
										+ tempEl.querySelector(".language").value
										+ ", "
										+ "languageTest: "
										+ tempEl
												.querySelector(".languageTest:not(.hidden)").value
										+ ", "
										+ "languageTestScore: "
										+ tempEl
												.querySelector(".languageTestScore").value
										+ ", "
										+ "languageTestScoreAcquisitionDate: "
										+ tempEl
												.querySelector(".languageTestScoreAcquisitionDate").value
										+ "; ";
								array.push(str);

							}

							document.querySelector("#languageTestScores").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>