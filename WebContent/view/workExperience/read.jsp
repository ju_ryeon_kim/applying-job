<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>경력사항</h1>
	<c:forEach var="workExperience" items="${ workExperiences }">
		<p>
			회사명: ${workExperience.companyName }
		</p>
		<p>
			근무기간:${workExperience.workStartDate } 
			- ${workExperience.workEndDate }
		</p>
		<p>
			근무부서: ${workExperience.departmentName }
		</p>
		<p>
			담당업무:  ${workExperience.workName }
		</p>
		<p>
			직위: ${workExperience.positionName }
		</p>
		<p>
			퇴직사유: ${workExperience.retirementReason }
		</p>
		<a href="/workExperience/delete/${workExperience.companyName }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/workExperience/write">[수정]</a>
	<a href="/workExperience/delete">[삭제]</a>
</body>
</html>