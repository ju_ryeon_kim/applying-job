<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>경력사항</h1>
	<form action="/workExperience/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="workExperiences" name="workExperiences" />
			<c:forEach var="workExperience" items="${ workExperiences }">
				<div class="workExperiences"">
					<p>
						회사명: <input type="text" class="companyName" value="${workExperience.companyName }" />
					</p>
					<p>
						근무기간: <input type="text" class="workStartDate" value="${workExperience.workStartDate } " />
						- <input type="text" class="workEndDate" value="${workExperience.workEndDate }" />
					</p>
					<p>
						근무부서: <input type="text" class="departmentName" value="${workExperience.departmentName }" />
					</p>
					<p>
						담당업무: <input type="text" class="workName" value="${workExperience.workName }" />
					</p>
					<p>
						직위: <input type="text" class="positionName" value="${workExperience.positionName }" />
					</p>
					<p>
						퇴직사유: <input type="text" class="retirementReason" value="${workExperience.retirementReason }" />
					</p>
					<input class="bTnDelete" type="button" value="삭제하기">
					<hr>
				</div>
			</c:forEach>
			<div id="template" style="display: none">
				<p>
					회사명: <input type="text" class="companyName" value="가나다회사" />
				</p>
				<p>
					근무기간: <input type="text" class="workStartDate" value="2004-03-02" />
					- <input type="text" class="workEndDate" value="2006-03-02" />
				</p>
				<p>
					근무부서: <input type="text" class="departmentName" value="개발부" />
				</p>
				<p>
					담당업무: <input type="text" class="workName" value="웹개발" />
				</p>
				<p>
					직위: <input type="text" class="positionName" value="팀원" />
				</p>
				<p>
					퇴직사유: <input type="text" class="retirementReason" value="개인사정" />
				</p>
				<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "workExperiences";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);

		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								var str = "companyName : "
										+ tempEl.querySelector(".companyName").value
										+ ", "
										+ "workStartDate: "
										+ tempEl
												.querySelector(".workStartDate").value
										+ ", "
										+ "workEndDate: "
										+ tempEl.querySelector(".workEndDate").value
										+ ", "
										+ "departmentName: "
										+ tempEl
												.querySelector(".departmentName").value
										+ ", "
										+ "workName: "
										+ tempEl.querySelector(".workName").value
										+ ", "
										+ "positionName: "
										+ tempEl.querySelector(".positionName").value
										+ ", "
										+ "retirementReason: "
										+ tempEl
												.querySelector(".retirementReason").value
										+ "; ";
								array.push(str);

							}

							document.querySelector("#workExperiences").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>