<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/view/common/global_header.jspf"%>
<%@ include file="/view/common/menu_header_write.jspf"%>
<h1>기본지원사항</h1>
<form action="/jobObjective/write" method="post">
	<p>
		지원분야: <input type="text" name="applyingField"
			value="${jobObjective.applyingField }" />
	</p>
	<p>
		현재연봉: <input type="text" name="annualSalary"
			value="${jobObjective.annualSalary }" />
	</p>
	<p>
		희망연봉: <input type="text" name="desiredAnnualSalary"
			value="${jobObjective.desiredAnnualSalary }" />
	</p>
	<hr>
	<input type="submit" value="수정하기" />
</form>
