<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>기본지원사항</h1>
	<form action="/jobObjective/write" method="post">
		<p>
			지원분야: <input type="text" name="applyingField" value="개발" />
		</p>
		<p>
			현재연봉: <input type="text" name="annualSalary" value="2400" />
		</p>
		<p>
			희망연봉: <input type="text" name="desiredAnnualSalary" value="2400" />
		</p>
		<hr>
		<input type="submit" value="등록하기" />
	</form>
</body>
</html>