<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>기본지원사항</h1>
	<p>지원분야: ${jobObjective.applyingField }</p>
	<p>현재연봉: ${jobObjective.annualSalary }</p>
	<p>희망연봉: ${jobObjective.desiredAnnualSalary }</p>
	<hr>
	<a href="/jobObjective/write">[수정]</a><a href="/jobObjective/delete">[삭제]</a>
</body>
</html>