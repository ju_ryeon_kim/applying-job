<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>자격증사항</h1>
	<c:forEach var="certificate" items="${ certificates }">
					<p>
				자격증: ${ certificate.certificateName}
				</p>
				<p>

					발행기관:${ certificate.certificateAuthority}
				</p>
				<p>
					취득일자: ${ certificate.certificateAcquisitionDate}
				</p>
			<a href="/certificate/delete/${certificate.certificateName }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/certificate/write">[수정]</a>
	<a href="/certificate/delete">[삭제]</a>
</body>
</html>