<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>자격증사항</h1>
	<form action="/certificate/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="certificates" name="certificates" />
			<c:forEach var="certificate" items="${ certificates }">
				<div class="certificates">
					<p>
						자격증: <input type="text" class="certificateName"
							value="${ certificate.certificateName}" />
					</p>
					<p>
						발행기관:<input type="text" class="certificateAuthority"
							value="${ certificate.certificateAuthority}" />
					</p>
					<p>
						취득일자:<input type="text" class="certificateAcquisitionDate"
							value="${ certificate.certificateAcquisitionDate}" />
					</p>
					<a href="/certificate/delete/${certificate.certificateName }">[삭제]</a>
					<hr>
				</div>
			</c:forEach>
			<div id="template" style="display: none">
				<p>
					자격증: <input type="text" class="certificateName" value="운전면허증" />
				</p>
				<p>

					발행기관: <input type="text" class="certificateAuthority" value="국토해양부" />
				</p>
				<p>
					취득일자: <input type="text" class="certificateAcquisitionDate"
						value="2004-03-02" />
				</p>
				<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "certificates";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);

		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								var str = "certificateName : "
										+ tempEl
												.querySelector(".certificateName").value
										+ ", "
										+ "certificateAuthority: "
										+ tempEl
												.querySelector(".certificateAuthority").value
										+ ", "
										+ "certificateAcquisitionDate: "
										+ tempEl
												.querySelector(".certificateAcquisitionDate").value
										+ "; ";
								array.push(str);

							}

							document.querySelector("#certificates").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>