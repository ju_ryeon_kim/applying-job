<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>IT활용능력</h1>
	<c:forEach var="applyingRoute" items="${ applyingRoutes }">
				<p>
					구분: ${applyingRoute.type }
				</p>
				<p>
					상세사항 기재: ${applyingRoute.applyingRoute }
				</p>
		<p>
			<a href="/applyingRoute/delete/${applyingRoute.applyingRoute }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/applyingRoute/write">[수정]</a>
	<a href="/applyingRoute/delete">[삭제]</a>
</body>
</html>