<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>IT활용능력</h1>
	<form action="/applyingRoute/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="applyingRoutes" name="applyingRoutes" />
			<c:forEach var="applyingRoute" items="${ applyingRoutes }">
				<div class="applyingRoutes">
					<p>
						구분: <select class="applyingRouteTypeId">
							<c:forEach var="applyingRouteType" items="${applyingRouteTypes }">
								<option value="${applyingRouteType.id }"
									${ applyingRoute.type == applyingRouteType.type ? "selected" : "" }>${applyingRouteType.type }</option>
							</c:forEach>
						</select>
					</p>
					<p>
						상세사항 기재: <input type="text" class="applyingRoute" value="네이버" />
					</p>
					<input class="bTnDelete" type="button" value="삭제하기">
					<hr>
				</div>
			</c:forEach>
			<div id="template" style="display: none">
				<p>
					구분: <select class="applyingRouteTypeId">
						<c:forEach var="applyingRouteType" items="${applyingRouteTypes }">
							<option value="${applyingRouteType.id }">${applyingRouteType.type }</option>
						</c:forEach>
					</select>
				</p>
				<p>
					상세사항 기재: <input type="text" class="applyingRoute" value="네이버" />
				</p>
				<p>
					<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "applyingRoutes";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);
		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								var str = "applyingRouteTypeId : "
										+ tempEl
												.querySelector(".applyingRouteTypeId").value
										+ ", "
										+ "applyingRoute: "
										+ tempEl
												.querySelector(".applyingRoute").value
										+ ", " + "; ";
								array.push(str);

							}

							document.querySelector("#applyingRoutes").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>