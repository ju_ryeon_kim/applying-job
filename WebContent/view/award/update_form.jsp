<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>수상경력</h1>
	<form action="/award/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="awards" name="awards" />
			<c:forEach var="award" items="${ awards }">
				<div class="awards">
					<p>
						수상명: <input type="text" class="awardName"
							value="${ award.awardName }" />
					</p>
					<p>
						수여기관: <input type="text" class="awardAuthority"
							value="${ award.awardAuthority }" />
					</p>
					<p>
						등급: <input type="text" class="awardGrade"
							value="${ award.awardGrade }" />
					</p>
					<p>
						취득일자: <input type="text" class="awardAcquisitionDate"
							value="${ award.awardAcquisitionDate }" />
					</p>
					<input class="bTnDelete" type="button" value="삭제하기">
					<hr>
				</div>
			</c:forEach>
			<div id="template" style="display: none">
				<p>
					수상명: <input type="text" class="awardName" value="가나다상" />
				</p>
				<p>
					수여기관: <input type="text" class="awardAuthority" value="가나다협회" />
				</p>
				<p>
					등급: <input type="text" class="awardGrade" value="우수" />
				</p>
				<p>
					취득일자: <input type="text" class="awardAcquisitionDate"
						value="2006-03-02" />
				</p>
				<p>
					<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "awards";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);

		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								
								var str = "awardName : "
										+ tempEl.querySelector(".awardName").value
										+ ", "
										+ "awardAuthority: "
										+ tempEl.querySelector(".awardAuthority").value
										+ ", "
										+ "awardGrade: "
										+ tempEl.querySelector(".awardGrade").value
										+ ", "
										+ "awardAcquisitionDate: "
										+ tempEl.querySelector(".awardAcquisitionDate").value
										+ "; ";
								array.push(str);
							}
							
							document.querySelector("#awards").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>