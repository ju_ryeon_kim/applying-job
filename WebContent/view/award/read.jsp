<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>수상경력</h1>
	<c:forEach var="award" items="${ awards }">
		<p>수상명: ${ award.awardName }</p>
		<p>수여기관: ${ award.awardAuthority }</p>
		<p>등급: ${ award.awardGrade }</p>
		<p>취득일자: ${ award.awardAcquisitionDate }</p>
		<p>
			<a href="/award/delete/${award.awardName }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/award/write">[수정]</a>
	<a href="/award/delete">[삭제]</a>
</body>
</html>