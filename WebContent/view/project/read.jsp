<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>IT활용능력</h1>
	<c:forEach var="project" items="${ projects }">
		<p>
			프로젝트명: ${project.project }
		</p>
		<p>
			대상기업: ${project.targetCompany }
		</p>
		<p>
			참여기간: ${project.startDate } - ${project.endDate }
		</p>
		<p>
			담당업무: ${project.responsibilities }
		</p>
		<p>
			사용기술: ${project.technology }
		</p>
		<p>
			<a href="/project/delete/${project.project }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/project/write">[수정]</a>
	<a href="/project/delete">[삭제]</a>
</body>
</html>