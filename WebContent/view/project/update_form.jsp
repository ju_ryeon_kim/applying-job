<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>IT활용능력</h1>
	<form action="/project/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="projects" name="projects" />
			<c:forEach var="project" items="${ projects }">
				<div class="projects">
					<p>
						프로젝트명: <input type="text" class="project"
							value="${project.project }" />
					</p>
					<p>
						대상기업: <input type="text" class="targetCompany"
							value="${project.targetCompany }" />
					</p>
					<p>
						참여기간: <input type="text" class="startDate"
							value="${project.startDate }" /> - <input type="text"
							class="endDate" value="${project.endDate }" />
					</p>
					<p>
						담당업무: <input type="text" class="responsibilities"
							value="${project.responsibilities }" />
					</p>
					<p>
						사용기술: <input type="text" class="technology"
							value="${project.technology }" />
					</p>
					<input class="bTnDelete" type="button" value="삭제하기">
					<hr>
				</div>
			</c:forEach>
			<div id="template" style="display: none">
				<p>
					프로젝트명: <input type="text" class="project" value="ABC프로젝트" />
				</p>
				<p>
					대상기업: <input type="text" class="targetCompany" value="삼성" />
				</p>
				<p>
					참여기간: <input type="text" class="startDate" value="2004-01-01" /> -
					<input type="text" class="endDate" value="2004-06-01" />
				</p>
				<p>
					담당업무: <input type="text" class="responsibilities" value="유지보수" />
				</p>
				<p>
					사용기술: <input type="text" class="technology" value="서블릿" />
				</p>
				<p>
					<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "projects";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);
		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								var str = "project : "
										+ tempEl.querySelector(".project").value
										+ ", "
										+ "targetCompany: "
										+ tempEl
												.querySelector(".targetCompany").value
										+ ", "
										+ "startDate: "
										+ tempEl.querySelector(".startDate").value
										+ ", "
										+ "endDate: "
										+ tempEl.querySelector(".endDate").value
										+ ", "
										+ "responsibilities: "
										+ tempEl
												.querySelector(".responsibilities").value
										+ ", "
										+ "technology: "
										+ tempEl.querySelector(".technology").value
										+ "; ";
								array.push(str);

							}

							document.querySelector("#projects").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>