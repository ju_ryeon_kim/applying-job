<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_read.jspf"%>
	<h1>IT활용능력</h1>
	<c:forEach var="careerStatement" items="${ careerStatements }">
		<p>회사명: ${ careerStatement.company }</p>
		<p>부서: ${ careerStatement.department }</p>
		<p>최종직급: ${ careerStatement.position }</p>
		<p>회사현황: ${ careerStatement.companyStatus }</p>
		<p>근무기간: ${ careerStatement.projectStartDate } - ${ careerStatement.projectEndDate }
		</p>
		<p>주요업무(프로젝트명): ${ careerStatement.mainBusiness }</p>
		<p>업무기간: ${ careerStatement.workStartDate } - ${ careerStatement.workEndDate }
		</p>
		<p>
			주요역할 및 담당:
			<textarea class="mainRole" rows="" cols="">${ careerStatement.mainRole }</textarea>
		</p>
		<p>
			업무성과:
			<textarea class="businessPerformance" rows="" cols="">${ careerStatement.businessPerformance }</textarea>
		</p>
		<p>
			<a href="/careerStatement/delete/${careerStatement.mainBusiness }">[삭제]</a>
		<hr>
	</c:forEach>
	<a href="/careerStatement/write">[수정]</a>
	<a href="/careerStatement/delete">[삭제]</a>
</body>
</html>