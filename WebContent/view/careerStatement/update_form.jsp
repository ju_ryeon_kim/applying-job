<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
.hidden {
	display: none;
}
</style>
</head>
<body>
	<%@ include file="/view/common/global_header.jspf"%>
	<%@ include file="/view/common/menu_header_write.jspf"%>
	<h1>경력기술서</h1>
	<form action="/careerStatement/write" method="post">
		<div id="createDiv">
			<input type="hidden" id="careerStatements" name="careerStatements" />
			<c:forEach var="careerStatement" items="${ careerStatements }">
				<div class="careerStatements">
					<p>
						회사명: <input type="text" class="company"
							value="${ careerStatement.company }" />
					</p>
					<p>
						부서: <input type="text" class="department"
							value="${ careerStatement.department }" />
					</p>
					<p>
						최종직급: <input type="text" class="position"
							value="${ careerStatement.position }" />
					</p>
					<p>
						회사현황: <input type="text" class="companyStatus"
							value="${ careerStatement.companyStatus }" />
					</p>
					<p>
						근무기간: <input type="text" class="projectStartDate"
							value="${ careerStatement.projectStartDate }" /> - <input
							type="text" class="projectEndDate"
							value="${ careerStatement.workEndDate }" />
					</p>
					<p>
						주요업무(프로젝트명): <input type="text" class="mainBusiness"
							value="${ careerStatement.mainBusiness }" />
					</p>
					<p>
						업무기간: <input type="text" class="workStartDate"
							value="${ careerStatement.workStartDate }" /> - <input
							type="text" class="workEndDate"
							value=" ${ careerStatement.workEndDate }" />
					</p>
					<p>
						주요역할 및 담당:
						<textarea class="mainRole" rows="" cols="">${ careerStatement.mainRole }</textarea>
					</p>
					<p>
						업무성과:
						<textarea class="businessPerformance" rows="" cols="">${ careerStatement.businessPerformance }</textarea>
					</p>
					<input class="bTnDelete" type="button" value="삭제하기">
					<hr>
				</div>
			</c:forEach>
			<div id="template" style="display: none">
				<p>
					회사명: <input type="text" class="company" value="ABC회사" />
				</p>
				<p>
					부서: <input type="text" class="department" value="가나다부서" />
				</p>
				<p>
					최종직급: <input type="text" class="position" value="팀장" />
				</p>
				<p>
					회사현황: <input type="text" class="companyStatus" value="" />
				</p>
				<p>
					근무기간: <input type="text" class="projectStartDate"
						value="2004-01-01" /> - <input type="text" class="projectEndDate"
						value="2004-06-01" />
				</p>
				<p>
					주요업무(프로젝트명): <input type="text" class="mainBusiness"
						value="ABC프로젝트" />
				</p>
				<p>
					업무기간: <input type="text" class="workStartDate" value="2004-01-01" />
					- <input type="text" class="workEndDate" value="2004-06-01" />
				</p>
				<p>
					주요역할 및 담당:
					<textarea class="mainRole" rows="" cols="">유지보수</textarea>
				</p>
				<p>
					업무성과:
					<textarea class="businessPerformance" rows="" cols=""></textarea>
				</p>
				<p>
					<input class="bTnDelete" type="button" value="삭제하기">
				<hr>
			</div>
			<input id="bTnAdd" type="button" value="추가하기">
			<hr>
		</div>
		<input type="submit" value="수정하기" />
	</form>
</body>
<script>
	window.onload = function() {
		var className = "careerStatements";
		var add = function addEducation(e) {
			var template = document.querySelector("#template");
			var newEl = template.cloneNode(true);
			newEl.id = "";
			newEl.className = className;
			newEl.style.display = "block";
			newEl.dataset.index = document.querySelectorAll("." + className).length;
			template.parentNode.insertBefore(newEl, template);
		};

		var deleteF = function(e) {
			if (e.target.className != "bTnDelete") {
				return;
			}
			var el = e.target.closest("." + className);
			el.parentNode.removeChild(el);
		};

		document.querySelector("#createDiv").addEventListener("click", deleteF,
				false);
		document.querySelector("#bTnAdd").addEventListener("click", add, false);
		document
				.querySelector("input[type=submit]")
				.addEventListener(
						"click",
						function(e) {
							//모든 education의 값을 json으로 만들어서 input.eduction에 넣는다.
							var array = [];
							var els = document
									.querySelectorAll("." + className);
							for ( var index in els) {
								if (!els.hasOwnProperty(index)) {
									continue;
								}
								var tempEl = els[index];
								var str = "company : "
										+ tempEl.querySelector(".company").value
										+ ", "
										+ "department: "
										+ tempEl.querySelector(".department").value
										+ ", "
										+ "position: "
										+ tempEl.querySelector(".position").value
										+ ", "
										+ "companyStatus: "
										+ tempEl
												.querySelector(".companyStatus").value
										+ ", "
										+ "projectStartDate: "
										+ tempEl
												.querySelector(".projectStartDate").value
										+ ", "
										+ "projectEndDate: "
										+ tempEl
												.querySelector(".projectEndDate").value
										+ ", "
										+ "mainBusiness: "
										+ tempEl.querySelector(".mainBusiness").value
										+ ", "
										+ "workStartDate: "
										+ tempEl
												.querySelector(".workStartDate").value
										+ ", "
										+ "workEndDate: "
										+ tempEl.querySelector(".workEndDate").value
										+ ", "
										+ "mainRole: "
										+ tempEl.querySelector(".mainRole").value
										+ ", "
										+ "businessPerformance: "
										+ tempEl
												.querySelector(".businessPerformance").value
										+ "; ";
								array.push(str);

							}

							document.querySelector("#careerStatements").value = array
									.join("");

						}, false);

	}
</script>
</body>
</html>