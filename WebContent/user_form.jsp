<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:choose>
		<c:when test="${not empty user}">
			<h1>회원정보수정</h1>
		</c:when>
		<c:otherwise>
			<h1>회원가입</h1>
		</c:otherwise>
	</c:choose>
	<c:set var="actionUrl" value="/user/create" />
	<c:if test="${not empty user}">
		<c:set var="actionUrl" value="/user/update" />
	</c:if>
	<form action="${actionUrl}" method="post">
		<p>
			아이디:
			<c:choose>
				<c:when test="${not empty user}">
	${user.userId}
					<input type="hidden" name="userId" value="${user.userId}" />
				</c:when>
				<c:otherwise>
					<input type="text" name="userId" />
				</c:otherwise>
			</c:choose>
		</p>
		<p>
			비밀번호: <input type="password" name="userPassword" />
		</p>
		<p>
			<c:choose>
				<c:when test="${not empty user}">
					<input type="submit" value="회원정보수정">
				</c:when>
				<c:otherwise>
					<input type="submit" value="회원가입">
				</c:otherwise>
			</c:choose>
		</p>
	</form>
</body>
</html>